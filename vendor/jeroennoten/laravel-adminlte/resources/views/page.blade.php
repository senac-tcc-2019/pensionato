@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">

    @stack('css')
    @yield('css')
    @yield('js')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li>
                            <p class="white mr-10">{{Session::get('boarding_name')}}</p>
                        </li>
                        <li>
                            <p class="white"><i class="fas fa-circle circle"></i>{{Auth::user()->name}}</p>
                        </li>
                        <li>
                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                            @else
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @if(session('status'))
            <div class="bs-callout bs-callout-{{session('status')}} bs-callout-lg">
                <h4 style="text-decoration: bold;">{{session('message')}}</h4>
            </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <div class="container-fluid">
                @include('layouts.modal')
                @yield('content')

            </div>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('.bs-callout').fadeOut(5000);
        });
    </script>

    @stack('js')
    @yield('js')
@stop
<style type="text/css">
    .white{
        color:white;
        margin-top: 15px;
    }

    .mr-10{
        margin-right: 10px;
    }

    .circle{
        width: 10px !important;
        height: 10px !important;
        color: green !important;
        border-radius: 100px !important;
        margin-right: 10px;
    }
    /* my own interpretation of Bootstrap callout */
    .bs-callout {
      padding: 20px;
      /*margin: 20px 0;*/
      border: 1px solid #eee;
      border-left-width: 5px;
      border-radius: 4px;
    }
    .bs-callout h4 {
      margin-top: 0;
      margin-bottom: 5px;
    }
    .bs-callout p:last-child {
      margin-bottom: 0;
    }
    .bs-callout code {
      border-radius: 4px;
    }
    .bs-callout+.bs-callout {
      margin-top: -5px;
    }
    .bs-callout-default {
      border-left-color: #777;
      background-color: #f7f7f9;
    }
    .bs-callout-default h4 {
      color: #777;
    }
    .bs-callout-primary {
      border-left-color: #428bca;
    }
    .bs-callout-primary h4 {
      color: #428bca;
    }
    .bs-callout-success {
      border-left-color: #5cb85c;
      background-color: #efffe8;
    }
    .bs-callout-success h4 {
      color: #5cb85c;
    }
    .bs-callout-danger {
      border-left-color: #d9534f;
      background-color: #fcf2f2;
    }
    .bs-callout-danger h4 {
      color: #d9534f;
    }
    .bs-callout-warning {
      border-left-color: #f0ad4e;
      background-color: #fefbed;
    }
    .bs-callout-warning h4 {
      color: #f0ad4e;
    }
    .bs-callout-info {
      border-left-color: #5bc0de;
      background-color: #f0f7fd;
    }
    .bs-callout-info h4 {
      color: #5bc0de;
    }
    /* bg transparency and disabled effects for Bootstrap callout */
    .bs-callout-default.transparent {
      background-color: rgb(247, 247, 249, 0.7); /*#f7f7f9*/
    }
    .bs-callout-success.transparent {
      background-color: rgb(239, 255, 232, 0.7); /*#efffe8*/
    }
    .bs-callout-warning.transparent {
      background-color: rgb(254, 251, 237, 0.7); /*#fefbed*/
    }
    .bs-callout-danger.transparent {
      background-color: rgb(252, 242, 242, 0.7); /*#fcf2f2*/
    }
    .bs-callout-info.transparent {
      background-color: rgb(240, 247, 253, 0.7); /*#f0f7fd*/
    }
    .bs-callout.disabled {
      opacity: 0.4;
    }
</style>
