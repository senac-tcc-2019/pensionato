<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => 'web'], function () {

        // 'middleware' => 'admin'
        Route::group(['prefix' => 'admin'], function () {

            Route::resource('/pensionato', 'BoardingSchoolController');
            Route::resource('/responsavel', 'ResponsableController');
            Route::resource('/pensionista', 'BoarderController');
            Route::resource('/quartos', 'BedroomController');
            Route::resource('/contrato', 'ContractController');
            Route::resource('/gastos-mensais', 'MonthlyBillController');
            Route::resource('/melhorias', 'ImprovementController');
            Route::resource('/funcionario', 'EmployeeController');
            Route::resource('/tarefas', 'TaskController');
            Route::resource('/contato', 'ContactController');
            Route::resource('/usuario', 'UserController');
            Route::resource('/regras', 'RuleController');

            Route::get('/melhorias-completas', 'ImprovementController@completed')->name('melhorias.completed');
            Route::get('/melhorias/to-complete/{id}', 'ImprovementController@complete')->name('melhorias.complete');
            Route::get('/melhorias-aprovadas', 'ImprovementController@approved')->name('melhorias.approved');
            Route::get('/melhorias/nao/aprovadas', 'ImprovementController@desaproved')->name('melhorias.desaproved');
            Route::get('/melhorias/to-approve/{id}', 'ImprovementController@approve')->name('melhorias.to.approve');
            Route::post('/melhorias/to-desaprove/{id}', 'ImprovementController@desapprove')->name('melhorias.to.desapprove');
            Route::get('/melhorias/check-duplication/{name}', 'ImprovementController@checkDuplication')->name('melhorias.check-duplication');

            Route::get('/home/pensionato', 'BoardingSchoolController@home')->name('boarding.home');

            Route::get('/configuracoes', 'ConfigController@create')->name('system.config');
            Route::get('/configuracoes/show/', 'ConfigController@show')->name('system.config.show');
            Route::post('/configuracoes/store', 'ConfigController@store')->name('system.config.store');
            Route::get('/configuracoes/edit/{id}', 'ConfigController@edit')->name('system.config.edit');
            Route::put('/configuracoes/update/{id}', 'ConfigController@update')->name('system.config.update');

            Route::get('/mensalidades', 'PaymentController@index')->name('payment.index');
            Route::get('/mensalidades/show/{id}', 'PaymentController@show')->name('payment.show');
            Route::get('/gerar', 'PaymentController@generate')->name('payment.generate');
        });


        //MIDDLEWARE PENSIONISTA
        Route::group(['middleware' => 'boarder', 'prefix' => 'pensionista'], function () {

        });

        //'middleware' => 'employee',
        //MIDDLEWARE FUNCIONÁRIO
        Route::group(['prefix' => 'funcionario'], function () {
            Route::get('/tarefas', 'EmployeeController@listTasksOfEmployee')->name('tasks.employee');
        });



        Route::get('/home', 'DashboardController@index')->name('home');
        Route::get('/modal', 'Controller@modal')->name('modal');
        Route::get('/modal/improvement', 'Controller@modalImprovement')->name('modal.improvement');
        Route::get('/modal/completed', 'Controller@modalCompleted')->name('modal.completed');
    });

});

Auth::routes();
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', function () {
    return redirect('/login');
});


