<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'cpf' => 'required',
            'rg' => 'required',
            'ctps' => 'required',
            'function' => 'required',
            'salary' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
        public function messages()
        {
            return [
                'name.required' => 'Nome do funcionário é um campo obrigatório',
                'phone.required' => 'Telefone do funcionário é um campo obrigatório',
                'cpf.required' => 'CPF de camas é um campo obrigatório',
                'ctps.required' => 'CTPS mensal do funcionário é um campo obrigatório',
                'function.required' => 'Função é um campo obrigatório',
                'salary.required' => 'Salário é um campo obrigatório'
            ];
        }
}
