<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResponsableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cpf' => 'required',
            'rg' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'facebook' => 'required',
            'whatsapp' => 'required',
            'cep' => 'required',
            'street' => 'required',
            'number' => 'required',
            'district' => 'required',
            'city' => 'required',
            'state' => 'required'
        ];
    }

  /**
 * Get the error messages for the defined validation rules.
 *
 * @return array
 */
    public function messages()
    {
        return [
            'name.required' => 'Nome do responsável é um campo obrigatório',
            'cpf.required' => 'CPF do responsável é um campo obrigatório',
            'rg.required' => 'RG do responsável é um campo obrigatório',
            'phone.required' => 'Telefone do responsável é um campo obrigatório',
            'email.required' => 'Email do responsável é um campo obrigatório',
            'facebook.required' => 'Facebook do responsável é um campo obrigatório',
            'whatsapp.required' => 'Whatsapp do responsável é um campo obrigatório',
            'cep.required' => 'CEP do responsável é um campo obrigatório',
            'street.required' => 'Rua do responsável é um campo obrigatório',
            'number.required' => 'Número do responsável é um campo obrigatório',
            'district.required' => 'Bairro do responsável é um campo obrigatório',
            'city.required' => 'Cidade do responsável é um campo obrigatório',
            'state.required' => 'Estado do responsável é um campo obrigatório',
        ];
    }
}
