<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BoardingSchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'responsable_id' => 'required',
            'cep' => 'required',
            'street' => 'required',
            'number' => 'required',
            'district' => 'required',
            'city' => 'required',
            'state' => 'required'

        ];
    }

  /**
 * Get the error messages for the defined validation rules.
 *
 * @return array
 */
    public function messages()
    {
        return [
            'name.required' => 'Nome do pensionato é um campo obrigatório',
            'responsable_id.required' => 'Responsável do pensionato é um campo obrigatório',
            'cep.required' => 'CEP é um campo obrigatório',
            'street.required' => 'Rua é um campo obrigatório',
            'number.required' => 'Número é um campo obrigatório',
            'district.required' => 'Bairro é um campo obrigatório',
            'city.required' => 'Cidade é um campo obrigatório',
            'state.required' => 'Estado é um campo obrigatório'
        ];
    }
}
