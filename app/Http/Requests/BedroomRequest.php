<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BedroomRequest extends FormRequest
{
   /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'area' => 'required',
            'bed' => 'required',
            'unit_price' => 'required',
            'description' => 'required'

        ];
    }

  /**
 * Get the error messages for the defined validation rules.
 *
 * @return array
 */
    public function messages()
    {
        return [
            'name.required' => 'Nome do quarto é um campo obrigatório',
            'area.required' => 'Área do quarto é um campo obrigatório',
            'bed.required' => 'Quantidade de camas é um campo obrigatório',
            'unit_price.required' => 'Valor mensal do quarto é um campo obrigatório',
            'description.required' => 'Descrição é um campo obrigatório',
        ];
    }
}
