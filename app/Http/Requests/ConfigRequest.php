<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'fund_percentage' => 'required',
            'interest_percentage' => 'required'
        ];
    }

     /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Título da melhoria é um campo obrigatório',
            'fund_percentage.required' => 'Porcentagem do fundo é um campo obrigatório',
            'interest_percentage.required' => 'Porcentagem dos juros é um campo obrigatório'
        ];
    }
}
