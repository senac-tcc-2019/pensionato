<?php

namespace App\Http\Controllers;

use App\Http\Requests\RuleRequest;
use App\Repositories\RuleRepository;

class RuleController extends Controller
{
    private $rule;

    public function __construct()
    {
        $this->rule = new RuleRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $rules = $this->rule->index();
            return view('admin.rules.index', compact('rules'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $boarding_schools = $this->rule->create();
            return view('admin.rules.create', compact('boarding_schools'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleRequest $request)
    {
        try {
            $this->rule->store($request);
            return redirect()->route('regras.index')->with('status', 'success')->with('message', 'Regra criada com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data = $this->rule->edit($id);
            $rule = $data[0];
            $boarding_schools = $data[1];

            return view('admin.rules.edit', compact('rule', 'boarding_schools'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RuleRequest $request, $id)
    {
        try {
            $this->rule->update($request, $id);
            return redirect()->route('regras.index')->with('status', 'success')->with('message', 'Regra alterada com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->rule->destroy($id);
            return redirect()->route('regras.index')->with('status', 'success')->with('message', 'Regra excluída com sucesso');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
