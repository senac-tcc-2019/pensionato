<?php

namespace App\Http\Controllers;

use App\Repositories\ContactRepository;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contact;

    public function __construct()
    {
        $this->contact = new ContactRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $contacts = $this->contact->index($request);
            return view('admin.contacts.index', compact('contacts'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.contacts.create');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        try {
            $this->contact->store($request);
            return redirect()->route('contato.index')->with('status', 'success')->with('message', 'Contato criado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try {
            $id = \Auth::user()->id;
            return $this->contact->show($id);
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $contact = $this->contact->edit($id);
            return view('admin.contacts.edit', compact('contact'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request, $id)
    {
        try {
            $this->contact->update($request, $id);
            return redirect()->route('contato.index')->with('status', 'success')->with('message', 'Contato alterado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
