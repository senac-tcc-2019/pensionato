<?php

namespace App\Http\Controllers;

use App\Repositories\DashboardRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DashboardRepository::index();
        $data = ['boardingPerState' => $datas['boardingPerState'], 'monthlyBillsPerYear' => $datas['monthlyBillsPerYear']];
        $data = json_encode($data);
        $paymentLatePerMonth = empty($datas['paymentLatePerMonth']) ? 0 : $datas['paymentLatePerMonth'];
        $paymentPerMonth =  empty($datas['paymentPerMonth']) ? 0 : $datas['paymentPerMonth'];
        $totalReceivedPaymentPerMonth = empty($datas['totalReceivedPaymentPerMonth']) ? 0 : $datas['totalReceivedPaymentPerMonth'];
        $totalLatePaymentPerMonth = empty($datas['totalLatePaymentPerMonth']) ? 0 : $datas['totalLatePaymentPerMonth'];

        return view('home', compact('data', 'paymentLatePerMonth', 'paymentPerMonth', 'totalLatePaymentPerMonth', 'totalReceivedPaymentPerMonth'));
    }
}
