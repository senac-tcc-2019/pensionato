<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\BoardingSchool;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function session($request = null, $id = null)
    {
        if($request and $id)
        {
            $request->session()->put('boarding_id', $id);
        }
        else
        {
            $request->session()->put('boarding_id', $request->session()->get('boarding_id'));
        }

        $boarding = BoardingSchool::find($request->session()->get('boarding_id'));
        $name = $boarding->name;

        $request->session()->put('boarding_name', $name);
    }

    public function modal(Request $request)
    {
        return view('layouts.modal_partial', ['title' => $request->title, 'route' => $request->route, 'item' => $request->item])->render();
    }

    public function modalImprovement(Request $request)
    {
        return view('layouts.modal_improvement', ['title' => $request->title, 'route' => $request->route, 'item' => $request->item, 'improvement' => $request->improvement])->render();
    }

    public function modalCompleted(Request $request)
    {
        return view('layouts.modal_completed', ['title' => $request->title, 'route' => $request->route, 'item' => $request->item, 'improvement' => $request->improvement])->render();
    }
}
