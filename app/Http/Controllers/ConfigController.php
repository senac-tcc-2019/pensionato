<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\ConfigRequest;
use App\Http\Controllers\Controller;
use App\Repositories\ConfigRepository;

class ConfigController extends Controller
{
    private $config;

    public function __construct()
    {
        $this->config = new ConfigRepository();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

            $user = \Auth::user()->id;
            $user = User::find($user);
            $user->login = $user->login + 1;
            $user->save();

            return view('admin.config.create');

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConfigRequest $request)
    {
        try {
            $this->config->store($request);
            return redirect()->route('home')->with('status', 'success')->with('message', 'Configuração criado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try {
            $id = \Auth::user()->id;
            $config = $this->config->show($id);
            return view('admin.config.show', compact('config'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $config = $this->config->edit($id);
            return view('admin.config.edit', compact('config'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConfigRequest $request, $id)
    {
        try {
            $this->config->update($request, $id);
            return redirect()->route('configuracoes.index')->with('status', 'success')->with('message', 'Configuração alterado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
