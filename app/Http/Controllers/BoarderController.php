<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\BoarderRepository;

class BoarderController extends Controller
{

    private $boarder;

    public function __construct()
    {
        $this->boarder = new BoarderRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $boarders = $this->boarder->index();
            return view('admin.boarders.index', compact('boarders'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {

            $data = $this->boarder->create($request);
            $responsables = $data[0];
            $bedrooms = $data[1];
            $contracts = $data[2];
            return view('admin.boarders.create', compact('responsables', 'bedrooms', 'contracts'));

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->boarder->store($request);
            return redirect()->route('pensionista.index')->with('status', 'success')->with('message', 'Pensionista criado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {

            $data = $this->boarder->edit($request, $id);
            $boarder = $data[0];
            $responsables = $data[1];
            $contracts = $data[2];
            $bedrooms = $data[3];

            return view('admin.boarders.edit', compact('boarder', 'responsables', 'contracts', 'bedrooms'));

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->boarder->update($request, $id);
            return redirect()->route('pensionista.index')->with('status', 'success')->with('message', 'Pensionista alterado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->boarder->destroy($id);
            return redirect()->route('pensionista.index')->with('status', 'success')->with('message', 'Pensionista excluído com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
