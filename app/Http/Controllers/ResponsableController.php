<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ResponsableRequest;
use App\Repositories\ResponsableRepository;
use App\Models\Responsable;

class ResponsableController extends Controller
{
    private $responsable;

    public function __construct(Request $request)
    {
        $this->responsable = new ResponsableRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $responsables = $this->responsable->index();
            return view('admin.responsables.index', compact('responsables'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

            $data = $this->responsable->create();
            $boarding_schools = $data[0];
            $boarders = $data[1];
            $users = $data[2];

            return view('admin.responsables.create', compact('boarding_schools', 'boarders', 'users'));

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResponsableRequest $request)
    {
        try {
            $this->responsable->store($request);
            return redirect()->route('responsavel.index')->with('status', 'success')->with('message', 'Responsável criado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data =  $this->responsable->edit($id);

            $responsable = $data[0];
            $boarding_schools = $data[1];
            $boarders = $data[2];
            $users = $data[3];

            return view('admin.responsables.edit', compact('responsable', 'boarding_schools', 'boarders', 'users'));

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ResponsableRequest $request, $id)
    {
        try {
            $this->responsable->update($request, $id);
            return redirect()->route('responsavel.index')->with('status', 'success')->with('message', 'Responsável alterado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->responsable->destroy($id);
            return redirect()->route('responsavel.index')->with('status', 'success')->with('message', 'Responsável excluído com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
