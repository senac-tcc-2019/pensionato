<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BoardingSchoolRequest;
use App\Repositories\BoardingSchoolRepository;

class BoardingSchoolController extends Controller
{
    private $boarder;

    public function __construct()
    {
        $this->boarder = new BoardingSchoolRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $boarding_schools = $this->boarder->index();
            return view('admin.boarding_school.index', compact('boarding_schools'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    public function home(Request $request)
    {
        try {

            if(\Auth::user()->login == 0 && \Auth::user()->role == 'administrador') {
                return redirect()->route('system.config');
            }

            $responsable = $this->boarder->home();
            return view('admin.boarding_school.home', compact('responsable'));

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $responsables = $this->boarder->create();
            return view('admin.boarding_school.create', compact('responsables'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BoardingSchoolRequest $request)
    {
        try {
            $boarding_school = $this->boarder->store($request);
            return redirect()->route('pensionato.index')->with('status', 'success')->with('message', 'Pensionato criado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            parent::session($request, $id);
            return redirect()->route('home');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data = $this->boarder->edit($id);
            $boarding_school = $data[0];
            $responsables = $data[1];

            return view('admin.boarding_school.edit', compact('boarding_school', 'responsables'));

        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BoardingSchoolRequest $request, $id)
    {
        try {
            return $this->boarder->update($request, $id);
            return redirect()->route('pensionato.index')->with('status', 'success')->with('message', 'Pensionato alterado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            return $this->boarder->destroy($id);
            return redirect()->route('pensionato.index')->with('status', 'success')->with('message', 'Pensionato excluído com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
