<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PaymentRepository;

class PaymentController extends Controller
{
    private $payment;

    public function __construct()
    {
        $this->payment = new PaymentRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $monthly_payment = $this->payment->index();
            return view('admin.payment.index', compact('monthly_payment'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    public function show($id)
    {
        return $this->payment->show($id)->renderHTML();
    }

    public function generate()
    {
        return $this->payment->generate();
    }
}
