<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BedroomRequest;
use App\Repositories\BedroomRepository;

class BedroomController extends Controller
{
    private $bedroom;

    public function __construct()
    {
        $this->bedroom = new BedroomRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $bedrooms = $this->bedroom->index($request);
            return view('admin.bedrooms.index', compact('bedrooms'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.bedrooms.create');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BedroomRequest $request)
    {
        try {
            $this->bedroom->store($request);
            return redirect()->route('quartos.index')->with('status', 'success')->with('message', 'Quarto criado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $bedroom = $this->bedroom->edit($id);
            return view('admin.bedrooms.edit', compact('bedroom'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BedroomRequest $request, $id)
    {
        try {
            $this->bedroom->update($request, $id);
            return redirect()->route('quartos.index')->with('status', 'success')->with('message', 'Quarto alterado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->bedroom->destroy($id);
            return redirect()->route('quartos.index')->with('status', 'success')->with('message', 'Quarto excluído com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
}
