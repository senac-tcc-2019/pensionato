<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ImprovementRequest;
use App\Repositories\ImprovementRepository;

class ImprovementController extends Controller
{
    private $improvement;

    public function __construct()
    {
        $this->improvement = new ImprovementRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = $this->improvement->index($request);
            $improvements = $data[1];
            $boarder= $data[0];

            return view('admin.improvements.index', compact('improvements', 'boarder'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.improvements.create');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImprovementRequest $request)
    {
        try {
         $this->improvement->store($request);
            return redirect()->route('melhorias.index')->with('status', 'success')->with('message', 'Melhoria criada com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $improvement = $this->improvement->edit($id);
            return view('admin.improvements.edit', compact('improvement'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImprovementRequest $request, $id)
    {
        try {
            $this->improvement->update($request, $id);
            return redirect()->route('melhorias.index')->with('status', 'success')->with('message', 'Melhoria alterada com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->improvement->destroy($id);
            return redirect()->route('melhorias.index')->with('status', 'success')->with('message', 'Melhoria excluída com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    public function desaproved()
    {
        try {
            $improvements = $this->improvement->desaproved();
            return view('admin.improvements.not-approved', compact('improvements'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }

    }

    public function desapprove(Request $request, $id)
    {
        return $this->improvement->desapprove($request, $id);
    }

    public function approved()
    {
        try {
            $improvements = $this->improvement->approved();
            return view('admin.improvements.approved', compact('improvements'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    public function approve($id)
    {
        return $this->improvement->approve($id);
    }

    public function completed()
    {
        try {
            $improvements = $this->improvement->completed();
            return view('admin.improvements.completed', compact('improvements'));
        } catch (\Exception $e) {
            return redirect()->back()->with('status', 'danger')->with('message', 'Erro. Tente novamente.');
        }
    }

    public function complete($id)
    {
        $this->improvement->complete($id);
    }

    public function checkDuplication($name)
    {
        return $this->improvement->checkDuplication($name);
    }
}
