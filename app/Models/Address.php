<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['cep', 'street', 'number', 'district', 'city', 'state'];

    public $timestamps = false; 

    public function responsable()
    {
        return $this->hasMany('App\Models\Responsable', 'addresses_id')->where('status', 1);
    }

    public function boarding_school()
    {
        return $this->hasOne('App\Models\BoardingSchool', 'addresses_id')->where('status', 1);
    }

    public function boarders()
    {
        return $this->hasMany('App\Models\Boarder', 'addresses_id')->where('status', 1);
    }

    public function employee() {
        return $this->hasOne('App\Models\Employee', 'addresses_id');
    }
}
