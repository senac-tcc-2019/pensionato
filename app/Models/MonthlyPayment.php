<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonthlyPayment extends Model
{
    protected $fillable = ['predicted_data', 'predicted_price', 'payment_date', 'payment_price', 'penalty', 'interest'];

    protected $table = 'monthly_payment';

    public $timestamps = false;

    public function boarder() {
        return $this->belongsTo('App\Models\Boarder', 'boarders_id');
    }
}
