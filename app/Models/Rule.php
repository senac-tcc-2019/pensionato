<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $fillable = ['rule', 'boarding_school_id', 'status'];

    public function boarding_school()
    {
        return $this->belongsTo('App\Models\BoardingSchool', 'boarding_school_id')->where('status', 1);
    }
}
