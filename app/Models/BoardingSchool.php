<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoardingSchool extends Model
{
    protected $fillable = ['name', 'status', 'responsables_id'];

    protected $table = 'boarding_school';

    public function responsable()
    {
        return $this->belongsTo('App\Models\Responsable', 'responsables_id')->where('status', 1);
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address', 'addresses_id')->where('status', 1);
    }

    public function rules()
    {
        return $this->hasMany('App\Models\Rule', 'boarding_school_id')->where('status', 1);
    }

    public function bedrooms()
    {
        return $this->belongsToMany('App\Models\Bedroom', 'boarding_school_has_bedrooms', 'bedrooms_id', 'boarding_school_id')->where('status', 1);
    }

    public function contact(){
        return $this->hasMany('App\Models\Contact');
    }
}
