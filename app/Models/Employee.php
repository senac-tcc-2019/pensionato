<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name', 'phone', 'cpf', 'rg', 'ctps', 'function', 'salary'];

    public function address() {
        return $this->belongsTo('App\Models\Address', 'addresses_id');
    }

    public function task() {
        return $this->hasMany('App\Models\Task', 'employees_id');
    }
}
