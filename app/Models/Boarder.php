<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boarder extends Model
{
    protected $fillable = ['name', 'cpf', 'rg', 'course', 'institution', 'phone', 'facebook', 'whatsapp', 'email', 'payment_day', 'status', 'responsables_id', 'bedrooms_id', 'contracts_id', 'users_id', 'addresses_id'];

    public function bedroom()
    {
        return $this->belongsTo('App\Models\Bedroom', 'bedrooms_id')->where('status', 1);
    }

    public function contract()
    {
        return $this->belongsTo('App\Models\Contract')->where('status', 1);
    }

    public function responsable()
    {
        return $this->belongsTo('App\Models\Responsable')->where('status', 1);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User')->where('status', 1);
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address', 'addresses_id')->where('status', 1);
    }

    public function payment()
    {
        return $this->hasOne('App\Models\MonthlyPayment', 'boarders_id');
    }
}
