<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = ['contract', 'duration', 'status'];

    public function boarders()
    {
        return $this->hasMany('App\Models\Boarder', 'contracts_id')->where('status', 1);
    }
}
