<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name', 'phone'];

    public function boarding_school(){
        return $this->belongsTo('App\Models\BoardingSchool');
    }
}
