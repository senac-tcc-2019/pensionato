<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'status', 'login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function responsable()
    {
    	return $this->belongsToMany('App\Models\Responsable', 'responsables_has_users', 'users_id', 'responsables_id')->where('status', 1);
    }

    public function boarders()
    {
    	return $this->hasMany('App\Models\Boarder', 'users_id')->where('status', 1);
    }

    public function settings() 
    {
        return $this->hasOne('App\Models\Config');
    }

    public function isAdmin()
    {
        if($this->role == 'administrador')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function isEmployee()
    {
        if($this->role == 'funcionario')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function isBoarder()
    {
        if($this->role == 'pensionista')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
