<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Responsable extends Model
{
    protected $fillable = ['name', 'cpf', 'rg', 'phone', 'facebook', 'whatsapp', 'email', 'status', 'addresses_id'];

    public function user()
    {
        return $this->belongsToMany('App\Models\User', 'responsables_has_users', 'responsables_id', 'users_id')->where('status', 1);
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address', 'addresses_id')->where('status', 1);
    }

    public function boarding_school()
    {
        return $this->hasMany('App\Models\BoardingSchool', 'responsables_id')->where('status', 1);
    }

    public function boarders()
    {
        return $this->hasMany('App\Models\Boarder', 'responsables_id')->where('status', 1);
    }
}
