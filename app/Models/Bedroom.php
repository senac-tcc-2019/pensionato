<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bedroom extends Model
{
    protected $fillable = ['name', 'description', 'area', 'bed', 'unit_price', 'status'];

    public function boarding_school()
    {
        return $this->belongsToMany('App\Models\BoardingSchool', 'boarding_school_has_bedrooms', 'boarding_school_id', 'bedrooms_id')->where('status', 1);
    }

    public function boarder()
    {
        return $this->hasOne('App\Models\Boarder', 'bedrooms_id');
    }

}
