<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Improvement extends Model
{
    protected $fillable = ['title', 'approved', 'completed', 'votes', 'reason', 'status', 'users_id'];

    public $timestamps = false;
}
