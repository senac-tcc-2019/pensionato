<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonthlyBill extends Model
{
    protected $fillable = ['name', 'value', 'pattern', 'status', 'boarding_school_id'];

    // public function boarder
}
