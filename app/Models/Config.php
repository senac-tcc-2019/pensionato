<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = ['title', 'fund_percentage', 'interest_percentage', 'fund', 'responsable_user_id'];

    protected $table = 'settings';

    public $timestamps = false;
    
    public function user() 
    {
        return $this->belongsTo('App\Models\User');
    }
} 
