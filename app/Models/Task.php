<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['description'];

    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employees_id');
    }
}
