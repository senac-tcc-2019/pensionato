<?php

namespace App\Repositories;

use App\Models\Bedroom;
use Illuminate\Http\Request;
use App\Http\Requests\BedroomRequest;

class BedroomRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->session()->get('boarding_id');
        $bedrooms = Bedroom::where('status', 1)->with([
            'boarding_school' => function($query) use ($id)
            {
                $query->where('id', $id);
            }
        ])->get();

        return $bedrooms;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BedroomRequest $request)
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $bedroom = Bedroom::create($request->all());
        $bedroom->boarding_school()->attach($request->session()->get('boarding_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bedroom = Bedroom::findOrFail($id);
        return $bedroom;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BedroomRequest $request, $id)
    {
       $bedroom = Bedroom::findOrFail($id);
       $bedroom->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bedroom = bedroom::findOrFail($id);
        $bedroom->status = 3;
        $bedroom->save();
    }
}
