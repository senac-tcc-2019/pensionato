<?php

namespace App\Repositories;

use App\Models\BoardingSchool;
use App\Models\Responsable;
use App\Models\Boarder;
use App\Models\Bedroom;
use App\Models\Contract;
use App\Models\Address;
use App\Models\User;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class BoarderRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boarders = Boarder::where('status', 1)->with('address')->get();
        return $boarders;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $id = $request->session()->get('boarding_id');

        $responsables = Responsable::where('status', 1)->get();
        $contracts = Contract::where('status', 1)->get();
        $bedrooms = Bedroom::where('status', 1)->with([
            'boarding_school' => function($query) use ($id)
            {
                $query->where('id', $id);
            }
        ])->get();

        return [$responsables, $bedrooms, $contracts];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->createUser($request);

        $address = Address::create($request->all());
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        //CRIANDO PENSIONISTA
        $boarder = new Boarder();
        $boarder->name = $request->name;
        $boarder->cpf = $request->cpf;
        $boarder->rg = $request->rg;
        $boarder->course = $request->course;
        $boarder->institution = $request->institution;
        $boarder->phone = $request->phone;
        $boarder->facebook = $request->facebook;
        $boarder->whatsapp = $request->whatsapp;
        $boarder->email = $request->email;
        $boarder->payment_day = $request->payment_day;
        $boarder->responsables_id = $request->responsable;
        $boarder->bedrooms_id = $request->bedroom;
        $boarder->contracts_id = $request->contract;
        $boarder->users_id = $user->id;
        $boarder->addresses_id = $address->id;
        $boarder->save();

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Request $request
     * @return User $user
     */
    private function createUser(Request $request) {
        $user_name = explode(' ', $request->name);
        $user_name = $user_name[0];

        $user = new User();
        $user->name = $user_name;
        $user->email = $request->email;
        $user->password = bcrypt($request->rg);
        $user->role = 'pensionista';
        $user->save();

        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($request, $id)
    {
        $boarder = Boarder::with('address', 'responsable', 'bedroom', 'contract')->findOrFail($id);

        $id = $request->session()->get('boarding_id');

        $responsables = Responsable::where('status', 1)->get();
        $contracts = Contract::where('status', 1)->get();
        $bedrooms = Bedroom::where('status', 1)->with([
            'boarding_school' => function($query) use ($id)
            {
                $query->where('id', $id);
            }
        ])->get();

        return [$boarder, $responsables, $contracts, $bedrooms];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $boarder = Boarder::findOrFail($id);
        $boarder->name = $request->name;
        $boarder->cpf = $request->cpf;
        $boarder->rg = $request->rg;
        $boarder->course = $request->course;
        $boarder->institution = $request->institution;
        $boarder->phone = $request->phone;
        $boarder->facebook = $request->facebook;
        $boarder->whatsapp = $request->whatsapp;
        $boarder->email = $request->email;
        $boarder->payment_day = $request->payment_day;
        $boarder->responsables_id = $request->responsable;
        $boarder->bedrooms_id = $request->bedroom;
        $boarder->contracts_id = $request->contract;
        $boarder->save();

        $boarder->address()->update($request->all());

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boarder = Boarder::findOrFail($id);
        $boarder->status = 3;
        $boarder->save();
    }
}
