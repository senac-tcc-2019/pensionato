<?php

namespace App\Repositories;

use Auth;
use App\Models\User;
use App\Models\Address;
use App\Models\Responsable;
use App\Models\BoardingSchool;
use Illuminate\Http\Request;

class BoardingSchoolRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::with('responsable')->find($id);

        $id_responsable = isset($user->responsable[0]->id) ? $user->responsable[0]->id : 0;

        $boarding_schools = BoardingSchool::with('responsable')
        ->where('responsables_id', $id_responsable)
        ->where('status', 1)
        ->get();

        if($user->role == 'administrador')
        {
            $boarding_school = BoardingSchool::with('responsable')
            ->where('status', 1)
            ->get();
        }

        return $boarding_schools;
    }

    public function home()
    {
        $id = Auth::id();
        $user = User::with('responsable')->find($id);
        $user->login = $user->login++;

        if(isset($user->responsable[0]))
        {
            $responsable = Responsable::with([
                'boarding_school' => function($query)
                {
                    $query->where('status', 1);
                }
            ])->find($user->responsable[0]->id);

            return $responsable;
        }

        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responsables = Responsable::where('status', 1)->get();
        return $responsables;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //SE VIER ID DO RESPONSÁVEL
        if($request->responsable_id != 0)
        {
            $responsable = Responsable::find($request->responsable_id);
        }
        //SE NÃO CRIA UM NOVO
        else
        {
            $address_responsable = new Address();
            $address_responsable->cep = $request->cep_responsable;
            $address_responsable->street = $request->street_responsable;
            $address_responsable->number = $request->number_responsable;
            $address_responsable->district = $request->district_responsable;
            $address_responsable->city = $request->city_responsable;
            $address_responsable->state = $request->state_responsable;
            $address_responsable->save();

            $responsable = $address_responsable->responsable()->create([
                'name' => $request->responsable_name,
                'cpf' => $request->cpf,
                'rg' => $request->rg,
                'phone' => $request->phone,
                'email' => $request->email,
                'facebook' => $request->facebook,
                'whatsapp' => $request->whatsapp
            ]);
        }

        $address_boarding_school = Address::create($request->all());
        $boarding_school = $address_boarding_school->boarding_school()->create([
            'name' => $request->name,
            'responsables_id' => $responsable->id
        ]);

        return $boarding_school;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boarding = BoardingSchool::findOrFail($id);
        $responsables = Responsable::where('status', 1)->get();

        return [$boarding, $responsables];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $boarding = BoardingSchool::findOrFail($id);

        if($request->responsable_id != 0)
        {
            $boarding->update([
                'name' => $request->name,
                'responsables_id' => $request->responsable_id
            ]);
        }
        else
        {
            $address_responsable = new Address();
            $address_responsable->cep = $request->cep_responsable;
            $address_responsable->street = $request->street_responsable;
            $address_responsable->number = $request->number_responsable;
            $address_responsable->district = $request->district_responsable;
            $address_responsable->city = $request->city_responsable;
            $address_responsable->state = $request->state_responsable;
            $address_responsable->save();

            $responsable = $address_responsable->responsable()->create([
                'name' => $request->responsable_name,
                'cpf' => $request->cpf,
                'rg' => $request->rg,
                'phone' => $request->phone,
                'email' => $request->email,
                'facebook' => $request->facebook,
                'whatsapp' => $request->whatsapp
            ]);

            $boarding->update([
                'name' => $request->name,
                'responsables_id' => $responsable->id
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boarding = BoardingSchool::findOrFail($id);
        $boarding->status = 3;
        $boarding->save();
    }
}
