<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Config;
use Illuminate\Http\Request;
use App\Http\Requests\ConfigRequest;

class ConfigRepository
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConfigRequest $request)
    {
        $user = User::find(\Auth::user()->id);

        $config = new Config();
        $config->title = $request->title;
        $config->fund_percentage = $request->fund_percentage;
        $config->interest_percentage = $request->interest_percentage;
        $config->improvements_percentage = $request->improvements_percentage;
        $config->responsable_users_id = $user->id;
        $config->fund = 0;
        $config->save();

        $user->login = $user->login + 1;
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $config = Config::where('responsable_users_id', $user->id)->get()->first();

        return $config;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Config::findOrFail($id);
        return $config;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConfigRequest $request, $id)
    {
        $config = Config::findOrFail($id);
        $config->update($request->all());
    }
}
