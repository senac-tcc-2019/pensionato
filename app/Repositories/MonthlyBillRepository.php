<?php

namespace App\Repositories;

use App\Models\MonthlyBill;
use Illuminate\Http\Request;
use App\Http\Requests\MonthlyBillRequest;

class MonthlyBillRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = MonthlyBill::where('status', 1)->get();
        return $bills;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MonthlyBillRequest $request)
    {
        $bills = MonthlyBill::create($request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bill = MonthlyBill::findOrFail($id);
        return $bill;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MonthlyBillRequest $request, $id)
    {
        $bill = MonthlyBill::findOrFail($id);
        $bill->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bill = MonthlyBill::findOrFail($id);
        $bill->status = 3;
        $bill->save();
    }
}
