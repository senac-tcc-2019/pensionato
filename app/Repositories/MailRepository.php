<?php


namespace App\Repositories;

use App\Models\MonthlyPayment;
use Mail;

class MailRepository
{
    /*
     * Get all payments that is near of the date of payment
     * and send mail to the boarder with boleto
     */
    public static function getPayments() {
        return MonthlyPayment::where('predicted_data', date('Y-m-d' , strtotime('+10 day')))
            ->where('payment_price', null)
            ->with('boarder')
            ->get();
    }
}
