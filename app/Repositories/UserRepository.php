<?php


namespace App\Repositories;


use App\Http\Requests\UserRequest;
use App\Models\User;

class UserRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('status', 1)->get();
        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->status = 3;
        $user->save();
    }

    /**
     * Validate if password has more than 2 letters and more than 4 numbers and if are equals
     *
     * @param  string $password
     * @param string $confirm
     * @return boolean
     */
    public function validatePassword($password, $confirm) {

        if($confirm =! $password) {
            return false;
        }

        $check = $this->validateCharacters($password, $confirm);

        if($check === false) {
            return false;
        }

        return true;
    }

    /**
     * Validate if password has more than 2 letters and more than 4 numbers
     *
     * @param  string $password
     * @param string $confirm
     * @return boolean
     */
    public function validateCharacters($password, $confirm) {

        $abc = [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z'
        ];
        $abc_count = 0;

        $numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
        $numbers_count = 0;

        $password = str_split($password);

        foreach($abc as $index) {
            if(in_array($index, $password)) {
                $abc_count++;
            }
        }

        foreach($numbers as $index) {
            if(in_array($index, $password)) {
                $numbers_count++;
            }
        }

        if($abc_count < 2 && $numbers_count < 4) {
            return false;
        }

        return true;
    }

    /**
     * Validate if email and users exist
     *
     * @param  string $email
     * @param string $name
     * @return boolean
     */
    public function validateUser($email, $name) {
        $users = User::where('name', $name)->where('email', $email)->get();

        if(count($users) > 0) {
            return false;
        }

        return true;
    }
}
