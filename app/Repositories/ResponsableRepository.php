<?php

namespace App\Repositories;

use App\Models\BoardingSchool;
use App\Models\Responsable;
use App\Models\Boarder;
use App\Models\Address;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ResponsableRequest;

class ResponsableRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsables = Responsable::where('status', 1)->with('address')->get();
        return $responsables;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boarding_schools = BoardingSchool::where('status', 1)->get();
        $boarders = Boarder::where('status', 1)->get();
        $users = User::where('role', 'administrador')->get();

        return [$boarding_schools, $boarders, $users];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResponsableRequest $request)
    {
        $address = Address::create($request->all());
        $responsable = $address->responsable()->create($request->all());

        if (isset($request->user_id)) {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0');
            $responsable->user()->attach($request->user_id);
            \DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }

        if (isset($request->boarding) and $request->boarder == 0 and $request->boarding = !0) {
            $boarding_school = BoardingSchool::findOrFail($request->boarding);
            $boarding_school->responsables_id = $responsable->id;
            $boarding_school->save();
        }

        if (isset($request->boarder) and $request->boarding == 0 and $request->boarder = !0) {
            $boarder = Boarder::findOrFail($request->boarder);
            $boarder->responsables_id = $responsable->id;
            $boarder->save();
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responsable = Responsable::with('address', 'user')->findOrFail($id);

        $boarding_schools = BoardingSchool::where('status', 1)->get();
        $boarders = Boarder::where('status', 1)->get();
        $users = User::where('role', 'administrador')->get();

        return [$responsable, $boarding_schools, $boarders, $users];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ResponsableRequest $request, $id)
    {
        $responsable = Responsable::findOrFail($id);

        $responsable->update($request->all());
        $responsable->address()->update($request->all());

        if (isset($request->user_id)) {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0');
            $responsable->user()->attach($request->user_id);
            \DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }

        if (isset($request->boarding) and $request->boarder == 0) {
            $boarding_school = BoardingSchool::findOrFail($request->boarding);
            $boarding_school->responsables_id = $responsable->id;
            $boarding_school->save();
        }


        if (isset($request->boarder) and $request->boarding == 0) {
            $boarder = Boarder::findOrFail($request->boarder);
            $boarder->responsables_id = $responsable->id;
            $boarder->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $responsable = Responsable::findOrFail($id);
        $responsable->status = 3;
        $responsable->save();
    }
}
