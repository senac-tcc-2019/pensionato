<?php

namespace App\Repositories;

use App\Models\MonthlyPayment;
use Illuminate\Http\Request;
use App\Models\Boarder;
use App\Models\Config;
use Carbon\Carbon;

class PaymentRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monthly_payment = MonthlyPayment::orderBy('predicted_data', 'DESC')->get();
        return $monthly_payment;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->createPayer();
        $this->createReceiver();
        $this->generatePDF($id);

        return $this->caixa;
    }

    private function createPayer() {
        $this->payer = new \Eduardokum\LaravelBoleto\Pessoa;
        $this->payer->setDocumento('00.000.000/0000-00');
        $this->payer->setNome('Weliton Barbosa Kuster');
        $this->payer->setCep('96015-160');
        $this->payer->setEndereco('General Argolo, 1024');
        $this->payer->setBairro('Centro');
        $this->payer->setUf('RS');
        $this->payer->setCidade('Pelotas');
    }

    private function createReceiver() {
        $this->receiver = new \Eduardokum\LaravelBoleto\Pessoa;
        $this->receiver->setDocumento('00.000.000/0000-00');
        $this->receiver->setNome('Francine Machado Moraes');
        $this->receiver->setCep('96015-160');
        $this->receiver->setEndereco('General Argolo, 1024');
        $this->receiver->setBairro('Centro');
        $this->receiver->setUf('RS');
        $this->receiver->setCidade('Pelotas');
    }

    private function generatePDF($id) {
        $payment = MonthlyPayment::find($id);
        $data = Carbon::parse($payment->predicted_data);

        $this->caixa = new \Eduardokum\LaravelBoleto\Boleto\Banco\Caixa;
        //$this->caixa->setLogo(asset('logo.jpg'));
        $this->caixa->setDataVencimento($data);
        //$this->caixa->setValor($payment->predicted_price);
        $this->caixa->setNumero(1);
        $this->caixa->setNumeroDocumento(1);
        $this->caixa->setPagador($this->payer);
        $this->caixa->setBeneficiario($this->receiver);
        $this->caixa->setCarteira('RG');
        $this->caixa->setAgencia(1111);
        $this->caixa->setCodigoCliente(222222);
    }
    public function generate()
    {
        $boarders = Boarder::with([
            'bedroom' => function ($query) {
                $query->with('boarding_school');
            },
        ])->where('status', 1)->get();

        $setting = Config::get()->first();

        foreach($boarders as $boarder)
        {
            $payment = new MonthlyPayment();

            $payment_day = date("d", strtotime($boarder->payment_day));
            $payment_month = date("m", strtotime($boarder->payment_day));
            $year = date("Y");

            $predicted_date = $year.'-'.$payment_month.'-'.$payment_day;

            $payment->predicted_data = $predicted_date;
            $payment->predicted_price = str_replace(",", ".", $boarder->bedroom->unit_price);
            $payment->interest = $setting->interest_percentage;
            $payment->boarders_id = $boarder->id;
            // $payment->payment_date = ;
            // $payment->payment_price = ;
            // $payment->penalty = $setting->penalty;
            $payment->save();
        }

        \Log::info('Monthly payment generate succesfully');
    }
}
