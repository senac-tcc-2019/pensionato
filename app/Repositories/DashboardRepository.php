<?php


namespace App\Repositories;


use Illuminate\Support\Facades\DB;

class DashboardRepository
{
    /**
        Return data to populate dashboard
     */
    public static function index()
    {
        return [
            'boardingPerState' => self::boardingPerState(),
            'paymentLatePerMonth' => self::paymentLatePerMonth(),
            'paymentPerMonth' => self::paymentPerMonth(),
            'totalReceivedPaymentPerMonth' => self::totalReceivedPaymentPerMonth(),
            'totalLatePaymentPerMonth' => self::totalLatePaymentPerMonth(),
            'monthlyBillsPerYear' => self::monthlyBillsPerYear()
        ];
    }

    private static function boardingPerState()
    {
        return DB::table('boarders')
            ->join('addresses', 'boarders.addresses_id', '=', 'addresses.id')
            ->select('addresses.state', DB::raw('COUNT(addresses.state) as quantity'))
            ->groupBy('addresses.state')
            ->get();
    }

    private static function paymentLatePerMonth()
    {
        return DB::table('monthly_payment')
            ->where('payment_date', '=!', null)
            ->count('id');
    }

    private static function paymentPerMonth()
    {
        return DB::table('monthly_payment')
            ->where('payment_date', '>', date('Y-m-d H:i:s'))
            ->avg('id');
    }

    private static function totalReceivedPaymentPerMonth()
    {
        return DB::table('monthly_payment')
            ->whereNotNull('payment_date')
            ->avg('payment_price');
    }

    private static function totalLatePaymentPerMonth()
    {
        return DB::table('monthly_payment')
            ->where('payment_date', '>', date('Y-m-d H:i:s'))
            ->avg('payment_price');
    }

    private static function monthlyBillsPerYear()
    {
        return DB::table('monthly_bills')
            ->select('name', 'value', DB::raw('YEAR(created_at) year'))
            ->get();
    }
}
