<?php

namespace App\Repositories;

use Auth;
use App\Models\Boarder;
use App\Models\KeyWord;
use App\Models\Improvement;
use Illuminate\Http\Request;
use App\Http\Requests\ImprovementRequest;

class ImprovementRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($request)
    {
        $boarding = $request->session()->get('boarding_id');

        $boarder = Boarder::with([
            'bedroom' => function($query) use ($boarding) {
                $query->with([
                    'boarding_school' => function($q) use ($boarding) {
                        $q->where('id', $boarding);
                    }
                ]);
            }
        ])->get()->count();

        $improvements = Improvement::where('status', '<', 3)
        ->get();

        return [$boarder, $improvements];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImprovementRequest $request)
    {
        $improvement = new Improvement();
        $improvement->title = $request->title;
        $improvement->users_id = Auth::user()->id;
        $improvement->save();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $improvement = Improvement::findOrFail($id);
        return $improvement;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImprovementRequest $request, $id)
    {
        $improvement = Improvement::findOrFail($id);
        $improvement->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $improvement = Improvement::findOrFail($id);
        $improvement->status = 3;
        $improvement->save();
    }

    public function desaproved()
    {
        return $improvements = Improvement::where('completed', 0)
        ->where('approved', 0)
        ->where('status', 2)
        ->get();
    }

    public function approved()
    {
        return $improvements = Improvement::where('approved', 1)
        ->where('status', 1)
        ->where('completed', 0)
        ->get();
    }

    public function approve($id)
    {
        $improvement = Improvement::findOrFail($id);
        $improvement->approved = 1;
        $improvement->status = 1;
        $improvement->save();
    }

    public function desapprove(Request $request, $id)
    {
        $improvement = Improvement::findOrFail($id);
        $improvement->approved = 0;
        $improvement->status = 2;
        $improvement->completed = 0;
        $improvement->reason = $request->reason;
        $improvement->save();
    }

    public function complete($id)
    {
        $improvement = Improvement::findOrFail($id);
        $improvement->approved = 1;
        $improvement->status = 1;
        $improvement->completed = 1;
        $improvement->save();
    }

    public function completed()
    {
        return $improvements = Improvement::where('approved', 1)
        ->where('status', 1)
        ->where('completed', 1)
        ->get();
    }

    public function checkDuplication($name)
    {
        $clean_word = $this->cleanName($name);
        $portion = explode(" ", $clean_word);
        $improvement = Improvement::query();

        foreach($portion as $p)
        {
            $improvement->Orwhere('title', 'like', '%', $p ,'%');
        }

        $improvement = $improvement->get();
        return $improvement;
    }

    private function cleanName($name) {
        $words = KeyWord::select('word')->get();
        $clean_word = " ".$name;
        $clean_word = str_replace(".", " ", $clean_word);

        foreach ($words as $key => $word) {
            $clean_word = str_replace(" ".$word->word." ", " ", $clean_word);
        }

        return $clean_word;
    }
}
