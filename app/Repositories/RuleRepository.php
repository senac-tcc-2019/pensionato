<?php

namespace App\Repositories;

use App\Models\BoardingSchool;
use App\Models\Rule;
use App\Http\Requests\RuleRequest;

class RuleRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rules = Rule::where('status', 1)->with('boarding_school')->get();
        return $rules;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleRequest $request)
    {
        $rule = new Rule();
        $rule->rule = $request->rule;
        $rule->boarding_school_id = $request->boarding_school_id;
        $rule->save();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rule = Rule::findOrFail($id);
        $boarding_school = BoardingSchool::where('status', 1)->get();

        return [$rule, $boarding_school];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boarding_school = BoardingSchool::where('status', 1)->get();
        return $boarding_school;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RuleRequest $request, $id)
    {
        $rule = Rule::findOrFail($id);
        $rule->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rule = Rule::findOrFail($id);
        $rule->status = 3;
        $rule->save();
    }
}
