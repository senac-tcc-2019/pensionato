<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\PaymentRepository;

class GeneratePayment extends Command
{
    private $payment;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate monthly payment.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->payment = new PaymentRepository();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->payment->generate();
    }
}
