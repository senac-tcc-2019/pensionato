<?php

namespace App\Console\Commands;

use Mail;
use App\Models\Config;
use App\Mail\SendMailable;
use Illuminate\Console\Command;
use App\Repositories\MailRepository;

class NotifyPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to notify payment of monthly payment via mail';

    private $payments;
    private $boleto;

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();

        $this->config = Config::first();
        $this->payments = MailRepository::getPayments();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->payments as $payment) {
            Mail::to($payment->boarder->email)->send(new SendMailable($payment, $this->config));
        }
    }
}
