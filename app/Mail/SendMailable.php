<?php

namespace App\Mail;

use App\Models\Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\MonthlyPayment;
use Illuminate\Queue\SerializesModels;
use App\Repositories\PaymentRepository;
use Eduardokum\LaravelBoleto\Boleto\Render\Pdf;
use Illuminate\Support\Facades\Storage;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    private $payment;
    private $config;
    private $boleto;
    private $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MonthlyPayment $payment, Config $config)
    {
        $this->boleto = new PaymentRepository();
        $this->payment = $payment;
        $this->config =  $config;
    }

    private function mountPayment($id) {
        $boleto = $this->boleto->show($id);

        $this->pdf = new Pdf();
        $this->pdf->addBoleto($boleto);
        $this->pdf = $this->pdf->gerarBoleto($this->pdf::OUTPUT_STRING);

    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->mountPayment($this->payment->id);

        $this->view('layouts.mail', ['payment' => $this->payment, 'config' => $this->config])
            ->subject('Pagamento de mensalidade')
            ->attachData($this->pdf, 'pagamento.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
