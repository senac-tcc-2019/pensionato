$(document).ready(function () {
    $('#dtBasicExample').DataTable({
        "language": {
            "lengthMenu": "AAAA",
            "zeroRecords": "Não foi encontrado nenhum registro.",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Não foi encontrado nenhum registro.",
            "search":         "Pesquisar:",
            "paginate": {
                "first":      "Primeira",
                "last":       "Última",
                "next":       "Próxima",
                "previous":   "Anterior"
            },
        }
    });
    $('.dataTables_length').addClass('bs-select');
    $('#dtBasicExample_length').hide();
});