<div class="form-group col-md-12">
    <label for="">Regra (*)</label>
    <textarea class="form-control" name="rule">{{(isset($rule->rule) ? $rule->rule : '')}}</textarea>
</div>
<div class="form-group col-md-12">
    <label for="">Pensionato (*)</label>
    <select name="boarding_school_id" class="form-control">
        <option>Selecione um pensionato</option>
        @foreach($boarding_schools as $boarding_school)
            <option value="{{$boarding_school->id}}" {{ (isset($rule) ? ($rule->boarding_school_id == $boarding_school->id ? 'selected' : '') : '') }}>{{$boarding_school->name}}</option>
        @endforeach
    </select>
</div>
