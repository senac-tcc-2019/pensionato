@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-external-link-alt"></i>Novo</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="box box-primary col-md-12 mt10">
        <h4>Contratos</h4>
        <br>
        <form action="{{route('contrato.store')}}" method="post">
            @csrf
            @include('admin.contracts._form')
            <div class="col-md-3">
                <button type="submit" class="w78 btn btn-success"><i class="far fa-save fa-2x"></i><br>Salvar</button>
                <a href="{{route('contrato.index')}}"><button type="button" class="w78 btn btn-danger"><i class="fas fa-times fa-2x"></i><br>Cancelar</button></a>
            </div>
        </form>
    </div>
@stop

@section('js')
<script src="{{asset('js/ckeditor5-build-classic/build/ckeditor.js')}}"></script>
<script>
$( document ).ready(function() {
    ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
});
</script>
@stop
