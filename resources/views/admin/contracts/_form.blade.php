<div class="new-contract">
    <div class="form-group col-md-12">
        <label for="">Contrato (*)</label>
        <textarea class="form-control" name="contract" id="editor">{!!isset($contract->contract) ? $contract->contract : ''!!}</textarea>
    </div>
    <div class="form-group col-md-12">
        <label for="">Duração do contrato (*)</label>
        <select class="form-control" name="duration">
            <option value="6 meses">6 meses</option>
            <option value="1 ano">1 ano</option>
        </select>
    </div>
</div>
