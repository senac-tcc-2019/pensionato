@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="box box-primary col-md-12 mt10">
        <div class="col-md-11">
            <h3>
                Configurações do sistema
                <a href="{{route('system.config.edit', Auth::user()->id)}}"><button style="float: right;" class="btn btn-success"><i class="fas fa-pencil-alt"></i></button></a>
            </h3>
            <br>
            <h4><strong>Título do sistema:</strong> {{$config->title}}</h4>
            <br>
            <h4><strong>Porcentagem do fundo:</strong> {{$config->fund_percentage}}</h4>
            <br>
            <h4><strong>Procentagem do juros:</strong> {{$config->interest_percentage}}</h4>
            <br>
            <h4><strong>Fundo:</strong> {{$config->fund}}</h4>
        </div>
    </div>
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="{{asset('js/mask.js')}}"></script>
@stop