<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="col-md-12">
                    <h3 style="text-align: center">Configurações iniciais do sistema</h3>
                </div>
                <form action="{{route('system.config.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="col-md-6" style="margin-top: 5%;">
                        <div class="form-group">
                            <label>Título do sistema (*)</label>
                            <input type="text" name="title" class="form-control" required placeholder="Nome que você deseja para o sistema" value="GerenciaPensionato">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Porcentagem do fundo (*)</label>
                            <p>A porcentagem do fundo refere-se no percentual desejado de todas as mensalidades pagas da casa para serem gastas apenas em melhorias para casa.</p>
                            <input class="form-control" name="fund_percentage" placeholder="Porcentagem em número" required type="number">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Porcentagem das melhorias (*)</label>
                            <p>A porcentagem das melhorias refere-se no percentual de quantidade desejado para que as melhorias possam ser aprovadas</p>
                            <input class="form-control" name="improvements_percentage" placeholder="Porcentagem em número" required type="number">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Porcentagem de juros (*)</label>
                            <p>A porcentagem de juros refere-se no percentual cobrado para cada dia atrasado no pagamento da mensalidade.</p>
                            <input class="form-control" name="interest_percentage" placeholder="Porcentagem em número" required type="number">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-secondary">Salvar</button>
                    </div>
                </form>
            </div>
        </main>
    </div>
</body>
