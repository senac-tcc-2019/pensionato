<div class="form-group col-md-7">
    <label for="">Título do sistema (*)</label>
    <input type="text" name="title" value="{{isset($config->title) ? $config->title : ''}}" class="form-control">
</div>
<div class="form-group col-md-7">
    <label for="">Porcentagem do fundo (*)</label>
    <input type="number" name="fund_percentage" value="{{isset($config->fund_percentage) ? $config->fund_percentage : ''}}" class="form-control">
</div>
<div class="form-group col-md-7">
    <label for="">Porcentagem dos juros (*)</label>
    <input type="number" name="interest_percentage" value="{{isset($config->interest_percentage) ? $config->interest_percentage : ''}}" class="form-control">
</div>
