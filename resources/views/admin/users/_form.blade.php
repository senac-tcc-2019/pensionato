 <div class="form-group col-md-12">
    <label for="">Nome (*)</label>
    <input type="text" required name="name" class="form-control" value="{{isset($user->name) ? $user->name : ''}}">
</div>
<div class="form-group col-md-12">
    <label for="">E-mail (*)</label>
    <input type="email" class="form-control" required name="email" value="{{isset($user->email) ? $user->email : ''}}">
</div>
 <div class="form-group col-md-12">
     <label for="">Permissão (*)</label>
     <select name="role" class="form-control">
         <option value="pensionista">Pensionista</option>
         <option value="funcionario">Funcionário</option>
         <option value="administrador">Administrador</option>
     </select>
 </div>
 <div class="form-group col-md-6">
     <label for="">Senha (*)</label>
     <input type="password" id="password" class="form-control" required name="password" value="{{isset($user->password) ? $user->password : ''}}">
 </div>
 <div class="form-group col-md-6">
     <label for="">Confirme a senha (*)</label>
     <input type="password" class="form-control" required name="confirm_password" value="{{isset($user->password) ? $user->password : ''}}">
 </div>
