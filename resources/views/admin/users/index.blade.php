@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas file-signature"></i>Usuários</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('usuario.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Nome</th>
                <th class="th-sm">E-mail</th>
                  <th class="th-sm">Permissão</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
              <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                  <td>{{$user->role}}</td>
                <td>
                  <a href="{{route('usuario.edit', $user->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del" data-item="{{$user->name}}" data-title="Usuário" data-route="{{route('usuario.destroy', $user->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop
