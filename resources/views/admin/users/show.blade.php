@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1>Visualizar usuário</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <style>
        #centro {
            width: 100%;
            height: 50px;
            text-align:center;
            position: absolute;
        }
    </style>
    <div class="box box-primary">
        <div id="centro" class="col-md-12">
            <img src="{{asset('/images/user.jpg')}}" width="150px" alt="Foto de usuário">
            <p>{{$user->name}}</p>
            <p>{{$user->role}}</p>
        </div>
    </div>
@stop
