<div class="box box-danger col-md-12">
    <h4><i class="fas fa-map-marked-alt mr10"></i>Endereço do responsável</h4>
    <div class="form-group col-md-12">
        <label for="">Cep (*)</label>
        <input type="text" id="cep2" name="cep_responsable" class="cep form-control">
    </div>
    <div class="form-group col-md-9">
        <label for="">Rua (*)</label>
        <input type="text" id="street2" name="street_responsable" class="street form-control">
    </div>
    <div class="form-group col-md-3">
        <label for="">Número (*)</label>
        <input type="text" id="number2" name="number_responsable" class="number form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Bairro (*)</label>
        <input type="text" id="district2" name="district_responsable" class="district form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Cidade (*)</label>
        <input type="text" id="city2" name="city_responsable" class="city form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Estado (*)</label>
        <input type="text" id="state2" name="state_responsable" class="state form-control">
    </div>
</div>
