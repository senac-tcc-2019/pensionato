@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-home"></i>Pensionatos</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('pensionato.create')}}"><button class="btn btn-primary">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Pensionato
                </th>
                <th class="th-sm">Responsável
                </th>
                <th class="th-sm">Rua
                </th>
                    <th class="th-sm">Nº
                </th>
                <th class="th-sm">Bairro
                </th>
                <th class="th-sm">Cidade
                </th>
                <th class="th-sm">Ações
                </th>
              </tr>
            </thead>
            <tbody>
            @foreach($boarding_schools as $boarding_school)
              <tr>
                <td>{{$boarding_school->name}}</td>
                <td>{{$boarding_school->responsable->name}}</td>
                <td>{{$boarding_school->address->street}}</td>
                <td>{{$boarding_school->address->number}}</td>
                <td>{{$boarding_school->address->district}}</td>
                <td>{{$boarding_school->address->city}}</td>
                <td>
                  <a href="{{route('pensionato.edit', $boarding_school->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$boarding_school->name}}" data-title="Pensionato" data-route="{{route('pensionato.destroy', $boarding_school->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop