@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-external-link-alt"></i>Novo</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="box box-primary col-md-12 mt10">
        <h4>Pensionato</h4>
        <br>
        <form action="{{route('pensionato.update', $boarding_school->id)}}" method="post">
            @method('put')
            @csrf
            @include('admin.boarding_school._form')
            @include('admin.boarding_school._form-address-boarding')
            @include('admin.boarding_school._form-responsable')
            <div class="col-md-3">
                <button type="submit" class="w78 btn btn-success"><i class="far fa-save fa-2x"></i><br>Salvar</button>
                <a href="{{route('pensionato.index')}}"><button type="button" class="w78 btn btn-danger"><i class="fas fa-times fa-2x"></i><br>Cancelar</button></a>
            </div>
        </form>
    </div>
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="{{asset('js/mask.js')}}"></script>
<script src="{{asset('js/cep.js')}}"></script>
<script>
    $( document ).ready(function() {

        $('.responsable').change(function(){
            var valor = $('.responsable').val();

            if(valor == 0) {
                $('.new-responsable').show();
                $(".responsable_name").prop('required',true);
                $(".cpf").prop('required',true);
                $(".rg").prop('required',true);
                $(".phone").prop('required',true);
                $(".email").prop('required',true);
                $(".facebook").prop('required',true);
                $(".cep").prop('required',true);
                $(".number").prop('required',true);
                $(".street").prop('required',true);
                $(".district").prop('required',true);
                $(".city").prop('required',true);
                $(".state").prop('required',true);
            }
            else
            {
                $('.new-responsable').hide();
                $(".responsable_name").prop('required',false);
                $(".cpf").prop('required',false);
                $(".rg").prop('required',false);
                $(".phone").prop('required',false);
                $(".email").prop('required',false);
                $(".facebook").prop('required',false);
                $(".cep").prop('required',false);
                $(".number").prop('required',false);
                $(".street").prop('required',false);
                $(".district").prop('required',false);
                $(".city").prop('required',false);
                $(".state").prop('required',false);
            }

        });

    });
</script>
@stop
