<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/boarding_school.css')}}">
    <title>Gerencia Pensionato</title>
</head>
<body>
    <div class="mt-15">
        <h1 class="text-center mt-5">Seja Bem-vindo(a) {{isset($responsable->name) ? $responsable->name : $user->name }}</h1>
        <div  class="box-max">
            @isset($responsable->boarding_school)
                @foreach($responsable->boarding_school as $boarding_school)
                    <div class="box-home border mr10 mt-5 box-max">
                        <a href="{{route('pensionato.show', $boarding_school->id)}}">
                            <i class="fas fa-home fa-5x" title="{{$boarding_school->name}}"></i>
                        </a>
                    </div>
                @endforeach
            @endisset
            {{-- <div class="box-home border mt-5 mr10 box-max">
                <a href="">
                    <i class="fas fa-home fa-5x"></i>
                </a>
            </div>
            <div class="box-home border mr10 mt-5 box-max">
                <a href="">
                    <i class="fas fa-home fa-5x"></i>
                </a>
            </div>
            <div class="box-home border mr10 mt-5 box-max">
                <a href="">
                    <i class="fas fa-home fa-5x"></i>
                </a>
            </div> --}}
            <div class="box-home box-max border mt-5 ">
                <a href="{{route('pensionato.create')}}">
                    <i class="fas fa-plus fa-5x" title="Novo"></i>
                </a>
            </div>
        </div>
    </div>
</body>
</html>