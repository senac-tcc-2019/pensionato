<div class="box box-success col-md-12">
    <h4><i class="fas fa-user-shield mr10"></i>Responsável</h4>
    <div class="form-group">
        <label for="">Selecione um responsável existente ou adicione ou novo (*)</label>
        <select required class="form-control responsable" name="responsable_id">
            <option>Selecione</option>
            <option value="0">Novo</option>
            @foreach($responsables as $responsable)
                @isset($boarding_school)
                    <option {{($boarding_school->responsable->id == $responsable->id ? 'selected' : '')}} value="{{$responsable->id}}">{{$responsable->name}}</option>
                @endisset
                @empty($boarding_school)
                    <option value="{{$responsable->id}}">{{$responsable->name}}</option>
                @endempty
            @endforeach
        </select>
    </div>
    <div class="new-responsable none">
        <div class="form-group col-md-12">
            <label for="">Nome do responsável (*)</label>
            <input type="text" name="responsable_name" class="form-control responsable_name">
        </div>
        <div class="form-group col-md-6">
            <label for="">CPF (*)</label>
            <input type="text" name="cpf" class="cpf form-control">
        </div>
        <div class="form-group col-md-6">
            <label for="">RG (*)</label>
            <input type="number" name="rg" class="rg form-control">
        </div>
        <div class="form-group col-md-6">
            <label for="">Telefone (*)</label>
            <input type="text" name="phone" class="phone form-control">
        </div>
        <div class="form-group col-md-6">
            <label for="">E-mail (*)</label>
            <input type="email" name="email" class="email form-control">
        </div>
        <div class="form-group col-md-6">
            <label for="">Facebook (*)</label>
            <input type="text" name="facebook" class="facebook form-control">
        </div>
        <div class="form-group col-md-6">
            <label for="">Whatsapp</label>
            <input type="text" name="whatsapp" class="phone form-control">
        </div>

        @include('admin.boarding_school._form-address-responsable')
    </div>
</div>
