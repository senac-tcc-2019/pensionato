<div class="box box-warning col-md-12">
    <h4><i class="fas fa-map-marked-alt mr10"></i>Endereço</h4>
    <div class="form-group col-md-12">
        <label for="">Cep (*)</label>
        <input required type="text" id="cep" name="cep" value="{{isset($boarding_school->address->cep) ? $boarding_school->address->cep : ''}}" class="cep form-control">
    </div>
    <div class="form-group col-md-9">
        <label for="">Rua (*)</label>
        <input required type="text" id="street" name="street" value="{{isset($boarding_school->address->street) ? $boarding_school->address->street : ''}}" class="form-control">
    </div>
    <div class="form-group col-md-3">
        <label for="">Número (*)</label>
        <input required type="text" id="number" name="number" value="{{isset($boarding_school->address->number) ? $boarding_school->address->number : ''}}" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Bairro (*)</label>
        <input required type="text" id="district" name="district" value="{{isset($boarding_school->address->district) ? $boarding_school->address->district : ''}}" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Cidade (*)</label>
        <input required type="text" id="city" name="city" value="{{isset($boarding_school->address->city) ? $boarding_school->address->city : ''}}" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Estado (*)</label>
        <input required type="text" id="state" name="state" value="{{isset($boarding_school->address->state) ? $boarding_school->address->state : ''}}" class="form-control">
    </div>
</div>
