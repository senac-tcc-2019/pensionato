@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas file-signature"></i>Funcionários</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('funcionario.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Nome</th>
                <th class="th-sm">Telefone</th>
                <th class="th-sm">CPF</th>
                <th class="th-sm">RG</th>
                <th class="th-sm">CTPS</th>
                <th class="th-sm">Função</th>                
                <th class="th-sm">Salário</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
              <tr>
                <td>{{$employee->name}}</td>
                <td>{{$employee->phone}}</td>
                <td>{{$employee->cpf}}</td>
                <td>{{$employee->rg}}</td>
                <td>{{$employee->ctps}}</td>
                <td>{{$employee->function}}</td>
                <td>{{$employee->salary}}</td>
                <td>
                  <a href="{{route('funcionario.edit', $employee->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$employee->name}}" data-title="Funcionário" data-route="{{route('funcionario.destroy', $employee->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop