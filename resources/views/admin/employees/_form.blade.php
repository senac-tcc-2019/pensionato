<div class="form-group col-md-8">
    <label for="">Nome (*)</label>
    <input type="text" name="name" class="form-control" value="{{isset($employee->name) ? $employee->name : ''}}" required>
</div>
<div class="form-group col-md-4">
    <label for="">Telefone (*)</label>
    <input type="text" name="phone" class="phone form-control" value="{{isset($employee->phone) ? $employee->phone : ''}}" required>
</div>
<div class="form-group col-md-4">
    <label for="">CPF (*)</label>
    <input type="text" name="cpf" class="form-control cpf" value="{{isset($employee->cpf) ? $employee->cpf : ''}}" required>
</div>
<div class="form-group col-md-4">
    <label for="">RG (*)</label>
    <input type="number" name="rg" class="form-control" value="{{isset($employee->rg) ? $employee->rg : ''}}" required>
</div>
<div class="form-group col-md-4">
    <label for="">CTPS (*)</label>
    <input type="text" name="ctps" class="form-control" value="{{isset($employee->ctps) ? $employee->ctps : ''}}" required>
</div>
<div class="form-group col-md-8">
    <label for="">Função (*)</label>
    <input type="text" name="function" class="form-control" value="{{isset($employee->function) ? $employee->function : ''}}" required>
</div>
<div class="form-group col-md-4">
    <label for="">Salário (*)</label>
    <input type="text" name="salary" class="form-control money" value="{{isset($employee->salary) ? $employee->salary : ''}}" required>
</div>
