@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-external-link-alt"></i>Novo</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    @include('admin.improvements.check-duplication')
    <div class="box box-primary col-md-12 mt10">
        <h4>Melhoria</h4>
        <br>
        <form action="{{route('melhorias.store')}}" class="form-class" method="post">
            @csrf
            @include('admin.improvements._form')
            <div class="col-md-3">
                <button type="button" class="w78 btn btn-success submit" data-toggle="modal" data-target="#myModal"><i class="far fa-save fa-2x"></i><br>Salvar</button>
                <a href="{{route('melhorias.index')}}"><button type="button" class="w78 btn btn-danger"><i class="fas fa-times fa-2x"></i><br>Cancelar</button></a>
            </div>
        </form>
    </div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $("#myModal").remove();
        $('#myModal2').prop('id', 'myModal');
    });

    $('.continue').click(function (){
        $( ".form-class" ).submit();
    });

    $('#title').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            event.preventDefault();
            checkDuplication();
        }
    });

    $('.submit').click(function (){
        checkDuplication();
    });

    function checkDuplication() {
        let name = $('#title').val();
        let route = '/admin/melhorias/check-duplication/'+name;

        $.ajax({
            type: 'GET',
            url: route,
            success: function (data){
                if(!data[0]){
                    $( ".form-class" ).submit();
                } else {
                    $('#myModal2').show();
                    for (var i = 0; i < data.length; i++) {
                        $(".list").append("<p>"+data[i].title+"</p>");
                    }
                }
            }
        });
    }
</script>
@stop
