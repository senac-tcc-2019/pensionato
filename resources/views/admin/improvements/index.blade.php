@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas file-signature"></i>Melhorias</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('melhorias.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <a href="{{route('melhorias.approved')}}"><button class="btn btn-primary mt-10">Aprovados</button></a>
        <a href="{{route('melhorias.desaproved')}}"><button class="btn btn-primary mt-10">Não aprovadas</button></a>
        <a href="{{route('melhorias.completed')}}"><button class="btn btn-primary mt-10">Completas</button></a>
        <br>
        <h3>Fundo xx,xx</h3>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Melhoria</th>
                <th class="th-sm">Status</th>
                <th class="th-sm">Completo</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($improvements as $improvement)
              <tr>
                <td>{{$improvement->title}}</td>
                <td>{{($improvement->approved == 0) ? 'Não aprovado' : 'Aprovado'}}</td>
                <td>{{($improvement->completed == 0) ? 'Não' : 'Sim'}}</td>
                <td>
                  <a href="{{route('melhorias.edit', $improvement->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del" data-item="{{$improvement->title}}" data-title="Melhoria" data-route="{{route('melhorias.destroy', $improvement->id)}}"><i class="fas fa-trash"></i></button>
                  @if($improvement->approved == 0)
                    <button class="btn btn-success js-btn-improvement" {{(($improvement->votes*60)/100) == (($boarder*60)/100) ? '' : 'disabled'}} data-improvement="aprovar" data-item="{{$improvement->title}}" data-title="Melhoria" data-route="{{route('melhorias.to.approve', $improvement->id)}}">
                      <i class="fas fa-check"></i>
                    </button>
                    <button class="btn btn-danger js-btn-improvement" {{(($improvement->votes*60)/100) == (($boarder*60)/100) ? '' : 'disabled'}} data-improvement="rejeitar" data-item="{{$improvement->title}}" data-title="Melhoria" data-route="{{route('melhorias.to.desapprove', $improvement->id)}}">
                      <i class="fas fa-times"></i>
                    </button>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop