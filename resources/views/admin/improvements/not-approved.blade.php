@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas file-signature"></i>Melhorias não aprovadas</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('melhorias.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <a href="{{route('melhorias.index')}}"><button class="btn btn-primary mt-10">Todas</button></a>
        <a href="{{route('melhorias.approved')}}"><button class="btn btn-primary mt-10">Aprovados</button></a>
        <a href="{{route('melhorias.desaproved')}}"><button class="btn btn-primary mt-10">Não aprovadas</button></a>
        <a href="{{route('melhorias.completed')}}"><button class="btn btn-primary mt-10">Completas</button></a>
        <br>
        <h3>Fundo xx,xx</h3>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Melhoria</th>
                <th class="th-sm">Motivo da rejeição</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($improvements as $improvement)
              <tr>
                <td>{{$improvement->title}}</td>
                <td>{{$improvement->reason}}</td>
                <td>
                  <a href="{{route('melhorias.edit', $improvement->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del" data-item="{{$improvement->title}}" data-title="Melhoria" data-route="{{route('melhorias.destroy', $improvement->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop