<div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="conteudo-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Possível duplicação de melhoria</h4>
                </div>
                <div class="modal-body">
                    <p>Por favor verifique se sua solicitação de melhoria, já consta em outra solicitação</p>
                    <div class="list">
                        <!-- Listar collections retornadas no ajax -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success continue">Continuar</button>
                    <button type="button" class="btn btn-danger" class="close" data-dismiss="modal">Cancelar</button>
                </div>      
            </div>
        </div>
    </div>
</div>