@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-money"></i>Mensalidades</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Boleto</th>
                <th class="th-sm">Data prevista</th>
                <th class="th-sm">Data de pagamento</th>
                <th class="th-sm">Valor previsto</th>
                <th class="th-sm">Valor do pagamento</th>
                <th class="th-sm">Multa</th>
                <th class="th-sm">Juros</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($monthly_payment as $payment)
              <tr>
                <td><a href="{{route('payment.show', $payment->id)}}" target="_blank"><button class="btn"><i title="Download Boleto" class="far fa-file-pdf"></i></button></a></td>
                <td>{{$payment->predicted_data}}</td>
                <td>{{$payment->payment_data}}</td>
                <td>{{$payment->predicted_price}}{{$payment->predicted_price > 0 ? ',00' : ''}}</td>
                <td>{{$payment->payment_price}}{{$payment->payment_price > 0 ? ',00' : ''}}</td>
                <td>{{$payment->penalty}}</td>
                <td>{{$payment->interest}}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop
