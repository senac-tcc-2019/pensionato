<div class="new-bedroom">
    <div class="form-group col-md-8">
        <label for="">Nome (*)</label>
        <input type="text" name="name" value="{{isset($bedroom->name) ? $bedroom->name : ''}}" required class="form-control name">
    </div>
    <div class="form-group col-md-4">
        <label for="">Área (*)</label>
        <input type="text" name="area" value="{{isset($bedroom->area) ? $bedroom->area : ''}}" required class="form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Camas (*)</label>
        <input type="number" name="bed" value="{{isset($bedroom->bed) ? $bedroom->bed : ''}}" required class="bed form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Valor (*)</label>
        <input type="text" name="unit_price" value="{{isset($bedroom->unit_price) ? $bedroom->unit_price : ''}}" required class="money form-control">
    </div>
    <div class="form-group col-md-12">
        <label for="">Descrição (*)</label>
        <textarea name="description" class="form-control" id="" cols="30" rows="5">{{isset($bedroom->description) ? $bedroom->description : ''}}</textarea>
    </div>
</div>
