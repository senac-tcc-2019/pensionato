@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-bed"></i>Quartos</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('quartos.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Nome</th>
                <th class="th-sm">Descrição</th>
                <th class="th-sm">Área</th>
                <th class="th-sm">Camas</th>
                <th class="th-sm">Valor</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($bedrooms as $bedroom)
              <tr>
                <td>{{$bedroom->name}}</td>
                <td>{{$bedroom->description}}</td>
                <td>{{$bedroom->area}}</td>
                <td>{{$bedroom->bed}}</td>
                <td>{{$bedroom->unit_price}}</td>
                <td>
                  <a href="{{route('quartos.edit', $bedroom->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$bedroom->name}}" data-title="Pensionista" data-route="{{route('quartos.destroy', $bedroom->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop