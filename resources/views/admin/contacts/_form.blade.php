 <div class="form-group col-md-12">
    <label for="">Nome do contato (*)</label>
    <input type="text" required name="name" class="form-control" value="{{isset($contact->name) ? $contact->name : ''}}">
</div>
<div class="form-group col-md-12">
    <label for="">Telefone (*)</label>
    <input type="text" class="form-control phone" required name="phone" value="{{isset($contact->phone) ? $contact->phone : ''}}">
</div>
