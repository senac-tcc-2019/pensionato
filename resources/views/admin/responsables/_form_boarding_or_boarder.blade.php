<div class="box box-success col-md-12">
    <h4><i class="fas fa-check mr10"></i>Atribuição</h4>
    <div class="form-group col-md-12">
        <label class="col-md-12" for="">Você deseja atribuir o responsável a um pensionato ou a um pensionista? (*)</label>

        <div class="col-md-6">
            <br>
            {{-- PENSIONISTA A QUEM O USUÁRIO É RESPONSAVEL --}}
            @isset($responsable->boarders[0])
                <label>Pensionistas a quem {{$responsable->name}} é responsável (*)</label>
                <br>
                @foreach($responsable->boarders as $boarder)
                <li>{{$boarder->name}}</li>
                    <br>
                @endforeach
                <br>
            @endisset
        </div>

        <div class="col-md-6">
            <br>
            {{-- PENSIONATOS A QUEM O USUÁRIO É RESPONSAVEL --}}
            @isset($responsable->boarding_school[0])
                <label>Pensionatos a quem {{$responsable->name}} é responsável (*)</label>
                <br>
                @foreach($responsable->boarding_school as $boarding)
                    <li>{{$boarding->name}}</li>
                    <br>
                @endforeach
                <br>
            @endisset
        </div>

        <br>
        <br>
        <div class="col-md-6">
            <i class="fas fa-user boarder"></i><label for="">Pensionista (*)</label>
            <input class="radio" type="radio" value="1" name="radio">
        </div>
        <div class="col-md-6">
            <i class="fas fa-home boarding"></i><label for="">Pensionato (*)</label>
            <input class="radio" type="radio" value="2" name="radio">
        </div>
    </div>
    <div class="form-group col-md-6 boarder-none none">
        <select name="boarder" class="form-control boarder">
            <option value="0" selected>Escolha um pensionista</option>
            @foreach($boarders as $boarder)
                <option value="{{$boarder->id}}">{{$boarder->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-6 boarding-none none ml-50">
        <select name="boarding" class="form-control boarding">
            <option value="0" selected>Escolha um pensionato</option>
            @foreach($boarding_schools as $boarding)
                <option value="{{$boarding->id}}">{{$boarding->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<style>
.ml-50
{
    margin-left: 50% !important;
}
</style>
