<div class="box box-warning col-md-12">
    <h4><i class="fas fa-map-marked-alt mr10"></i>Endereço do responsável</h4>
    <div class="form-group col-md-12">
        <label for="">Cep (*)</label>
        <input type="text" id="cep" name="cep" required value="{{isset($responsable->address->cep) ? $responsable->address->cep : ''}}" class="cep form-control">
    </div>
    <div class="form-group col-md-9">
        <label for="">Rua (*)</label>
        <input type="text" id="street" name="street" value="{{isset($responsable->address->street) ? $responsable->address->street : ''}}" required class="street form-control">
    </div>
    <div class="form-group col-md-3">
        <label for="">Número (*)</label>
        <input type="text" id="number" name="number" value="{{isset($responsable->address->number) ? $responsable->address->number : ''}}" required class="number form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Bairro (*)</label>
        <input type="text" id="district" value="{{isset($responsable->address->district) ? $responsable->address->district : ''}}" name="district" required class="district form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Cidade (*)</label>
        <input type="text" id="city" name="city" value="{{isset($responsable->address->city) ? $responsable->address->city : ''}}" required class="city form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="">Estado (*)</label>
        <input type="text" id="state" name="state" value="{{isset($responsable->address->state) ? $responsable->address->state : ''}}" required class="state form-control">
    </div>
</div>
