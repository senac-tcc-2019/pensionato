@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-user-shield"></i>Responsáveis</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
      <a href="{{route('responsavel.create')}}"><button class="btn btn-primary">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Nome</th>
                <th class="th-sm">Telefone</th>
                <th class="th-sm">Whatsapp</th>
                <th class="th-sm">Rua</th>
                <th class="th-sm">Nº</th>
                <th class="th-sm">Bairro</th>
                <th class="th-sm">Cidade</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($responsables as $responsable)
              <tr>
                <td>{{$responsable->name}}</td>
                <td>{{$responsable->phone}}</td>
                <td>{{$responsable->whatsapp}}</td>
                <td>{{$responsable->address->street}}</td>
                <td>{{$responsable->address->number}}</td>
                <td>{{$responsable->address->district}}</td>
                <td>{{$responsable->address->city}}</td>
                <td>
                  <a href="{{route('responsavel.edit', $responsable->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$responsable->name}}" data-title="Responsável" data-route="{{route('responsavel.destroy', $responsable->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop