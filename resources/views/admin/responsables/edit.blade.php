@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-external-link-alt"></i>Editar</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="box box-primary col-md-12 mt10">
        <h4>Responsável</h4>
        <br>
        <form action="{{route('responsavel.update', $responsable->id)}}" method="post">
            @method('put')
            @csrf
            @include('admin.responsables._form')
            @include('admin.responsables._form_address')
            @include('admin.responsables._form_boarding_or_boarder')
            @include('admin.responsables._form_users')
            <div class="col-md-3">
                <button type="submit" class="w78 btn btn-success"><i class="far fa-save fa-2x"></i><br>Salvar</button>
                <a href="{{route('responsavel.index')}}"><button type="button" class="w78 btn btn-danger"><i class="fas fa-times fa-2x"></i><br>Cancelar</button></a>
            </div>
        </form>
    </div>
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="{{asset('js/mask.js')}}"></script>
<script src="{{asset('js/cep.js')}}"></script>
<script>
    $( document ).ready(function() {

        $('.radio').click(function() {

            $('.radio').on('change', function() {

                let value = $('input[name=radio]:checked').val();

                if(value == 1) {
                    $('.boarder-none').show();
                    $('.boarding-none').hide();

                    $(".boarder").prop('required',true);
                    $(".boarding").prop('required',false);
                }
                else if(value == 2){
                    $('.boarding-none').show();
                    $('.boarder-none').hide();

                    $(".boarding").prop('required',true);
                    $(".boarder").prop('required',false);
                }

            });
        });

    });
    </script>
@stop
