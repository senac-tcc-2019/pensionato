<div class="box box-warning col-md-12">
    <h4><i class="fas fa-check mr10"></i>Vincular Usuário</h4>
    <div class="form-group col-md-12">
        <label class="col-md-12" for="">A que usuário este responsável está vinculado? (*)</label>

        <div class="col-md-6">
            <br>
            {{-- USUÁRIO QUE O ESTE RESPONSÁVEL ESTÁ VINCULADO --}}
            @isset($users)
                <label>Usuários (*)</label>
                <select required class="form-control" name="user_id">
                    <option>Escolha um usuário</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}" {{isset($responsable->user[0]->id)  ? $responsable->user[0]->id == $user->id ? 'selected' : '' : ''}}>{{$user->name}}</option>
                    @endforeach
                </select>
            @endisset
        </div>
    </div>
</div>
