<div class="new-responsable">
    <div class="form-group col-md-12">
        <label for="">Nome do responsável (*)</label>
        <input type="text" name="name" value="{{isset($responsable->name) ? $responsable->name : ''}}" required class="form-control name">
    </div>
    <div class="form-group col-md-6">
        <label for="">CPF (*)</label>
        <input type="text" name="cpf" value="{{isset($responsable->cpf) ? $responsable->cpf : ''}}" required class="cpf form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">RG (*)</label>
        <input type="number" name="rg" value="{{isset($responsable->rg) ? $responsable->rg : ''}}" required class="rg form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Telefone (*)</label>
        <input type="text" name="phone" value="{{isset($responsable->phone) ? $responsable->phone : ''}}" required class="phone form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">E-mail (*)</label>
        <input type="email" name="email" value="{{isset($responsable->email) ? $responsable->email : ''}}" required class="email form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Facebook (*)</label>
        <input type="text" name="facebook" value="{{isset($responsable->facebook) ? $responsable->facebook : ''}}" required class="facebook form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Whatsapp (*)</label>
        <input type="text" name="whatsapp" value="{{isset($responsable->whatsapp) ? $responsable->whatsapp : ''}}" required class="phone form-control">
    </div>
</div>
