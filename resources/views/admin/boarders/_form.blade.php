<div class="new-boarder">
    <div class="form-group col-md-8">
        <label for="">Nome (*)</label>
        <input type="text" name="name" value="{{isset($boarder->name) ? $boarder->name : ''}}" required class="form-control name">
    </div>
    <div class="form-group col-md-4">
        <label for="">Data de pagamento da mensalidade (*)</label>
        <input type="date" name="payment_day" value="{{isset($boarder->payment_day) ? $boarder->payment_day : ''}}" required class="rg form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">CPF (*)</label>
        <input type="text" name="cpf" value="{{isset($boarder->cpf) ? $boarder->cpf : ''}}" required class="cpf form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">RG (*)</label>
        <input type="number" name="rg" value="{{isset($boarder->rg) ? $boarder->rg : ''}}" required class="rg form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Curso (*)</label>
        <input type="text" name="course" value="{{isset($boarder->course) ? $boarder->course : ''}}" required class="rg form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Instituição (*)</label>
        <input type="text" name="institution" value="{{isset($boarder->institution) ? $boarder->institution : ''}}" required class="rg form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Telefone (*)</label>
        <input type="text" name="phone" value="{{isset($boarder->phone) ? $boarder->phone : ''}}" required class="phone form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">E-mail (*)</label>
        <input type="email" name="email" value="{{isset($boarder->email) ? $boarder->email : ''}}" required class="email form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Facebook (*)</label>
        <input type="text" name="facebook" value="{{isset($boarder->facebook) ? $boarder->facebook : ''}}" required class="facebook form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Whatsapp (*)</label>
        <input type="text" name="whatsapp" value="{{isset($boarder->whatsapp) ? $boarder->whatsapp : ''}}" required class="phone form-control">
    </div>
</div>
