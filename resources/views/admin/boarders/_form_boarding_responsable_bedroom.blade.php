<div class="box box-success col-md-12">
    <h4><i class="fas fa-check mr10"></i>Atribuição</h4>
    <div class="form-group col-md-12">
        <br>
        <div class="form-group col-md-12">
            <label for="">Responsável (*)</label>
            <select name="responsable" class="form-control">
                <option value="0" selected>Escolha um responsável</option>
                @foreach($responsables as $responsable)
                    @isset($boarder)
                        <option value="{{$responsable->id}}" {{($boarder->responsables_id == $responsable->id) ? 'selected' : ''}}>{{$responsable->name}}</option>
                    @endisset
                    @empty($boarder)
                        <option value="{{$responsable->id}}">{{$responsable->name}}</option>
                    @endempty
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-12">
            <label for="">Dormitórios (*)</label>
            <select name="bedroom" class="form-control">
                <option value="0" selected>Escolha um dormitório</option>
                @foreach($bedrooms as $bedroom)
                    @isset($boarder)
                        <option value="{{$bedroom->id}}" {{($boarder->bedrooms_id == $bedroom->id) ? 'selected' : ''}}>{{$bedroom->name}}</option>
                    @endisset
                    @empty($boarder)
                        <option value="{{$bedroom->id}}">{{$bedroom->name}}</option>
                    @endempty
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-12">
            <label for="">Contrato (*)</label>
            <select name="contract" class="form-control">
                <option value="0" selected>Escolha um contrato</option>
                @foreach($contracts as $contract)
                    @isset($boarder)
                        <option value="{{$contract->id}}" {{($boarder->contracts_id == $contract->id) ? 'selected' : ''}}>{{$contract->duration}}</option>
                    @endisset
                    @empty($boarder)
                        <option value="{{$contract->id}}">{{$contract->duration}}</option>
                    @endempty
                @endforeach
            </select>
        </div>
    </div>
</div>
