@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-user"></i>Pensionista</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('pensionista.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Nome</th>
                <th class="th-sm">Curso</th>
                <th class="th-sm">Telefone</th>
                <th class="th-sm">Cidade</th>
                <th class="th-sm">Estado</th>
                <th class="th-sm">Data de pagamento</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($boarders as $boarder)
              <tr>
                <td>{{$boarder->name}}</td>
                <td>{{$boarder->course}}</td>
                <td>{{$boarder->phone}}</td>
                <td>{{$boarder->address->city}}</td>
                <td>{{$boarder->address->state}}</td>
                <td>{{$boarder->payment_day}}</td>
                <td>
                  <a href="{{route('pensionista.edit', $boarder->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$boarder->name}}" data-title="Pensionista" data-route="{{route('pensionista.destroy', $boarder->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop