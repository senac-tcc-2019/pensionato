@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas file-signature"></i>Tarefas</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('tarefas.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Descrição</th>
                <th class="th-sm">Funcionário</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
              <tr>
                <td>{!!$task->description!!}</td>
                <td>{{$task->employee->name}}</td>
                <td>
                  <a href="{{route('tarefas.edit', $task->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$task->description}}" data-title="Contrato" data-route="{{route('tarefas.destroy', $task->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop
