<div class="form-group col-md-12">
    <label for="">Descrição (*)</label>
    <textarea class="form-control" name="description">{{(isset($task->description) ? $task->description : '')}}</textarea>
</div>
<div class="form-group col-md-12">
    <label for="">Funcionário (*)</label>
    <select name="employees_id" class="form-control">
        <option>Selecione um funcionário</option>
        @foreach($employees as $employee)
            <option value="{{$employee->id}}" {{ (isset($task) ? ($task->employees_id == $employee->id ? 'selected' : '') : '') }}>{{$employee->name}}</option>
        @endforeach
    </select>
</div>
