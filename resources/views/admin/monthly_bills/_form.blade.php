<div class="new-bill">
    <div class="form-group col-md-6">
        <label for="">Nome da conta (*)</label>
        <input type="text" name="name" value="{{isset($bill->name) ? $bill->name : ''}}" class="form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Valor (*)</label>
        <input type="text" name="value" value="{{isset($bill->value) ? $bill->value : ''}}" class="money form-control">
    </div>
    <div class="form-group col-md-12">
        <label for="">Padrão (*)</label>
        <select class="form-control" name="pattern">
            <option {{isset($bill->pattern) and $bill->pattern == 1 ? 'selected' : ''}} value="1">Sim</option>
            <option {{isset($bill->pattern) and $bill->pattern == 0 ? 'selected' : ''}} value="0">Não</option>
        </select>
    </div>
    <input type="hidden" value="{{session()->get('boarding_id')}}" name="boarding_school_id">
</div>
