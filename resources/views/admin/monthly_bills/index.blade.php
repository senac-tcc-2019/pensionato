@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas fa-money"></i>Gastos mensais</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <a href="{{route('gastos-mensais.create')}}"><button class="btn btn-primary mt-10">Novo</button></a>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Nome</th>
                <th class="th-sm">Valor</th>
                <th class="th-sm">Padrão</th>
                <th class="th-sm">Ações</th>
              </tr>
            </thead>
            <tbody>
            @foreach($bills as $bill)
              <tr>
                <td>{{$bill->name}}</td>
                <td>{{$bill->value}}</td>
                <td>{{($bill->pattern == 1) ? 'Sim' : 'Não'}}</td>
                <td>
                  <a href="{{route('gastos-mensais.edit', $bill->id)}}">
                    <button class="btn btn-success">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  <button class="btn btn-danger js-btn-del"  data-item="{{$bill->name}}" data-title="Gastos mensais" data-route="{{route('gastos-mensais.destroy', $bill->id)}}"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop 

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop