@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('content_header')
    <h1><i class="fas file-signature"></i>Tarefas</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/app-boarding.css')}}">
@stop
@section('content')
    <div class="col-md-12 mt10">
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">Descrição</th>
                <th class="th-sm">Casa</th>
              </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
              <tr>
                <td>{!!$task->description!!}</td>
                <td>{{$task->boarding_school->name}}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
    </div>
@stop

@section('js')
<script src="{{asset('js/boarding.js')}}"></script>
@stop
