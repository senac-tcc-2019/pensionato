@extends('adminlte::page')

@section('title', 'Gerencia Pensionato')

@section('js')
    <head>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            let boardingPerState = [['Task', 'Hours per Day']];
            let monthlyBillsPerYear = [];
            let datas = <?php echo isset($data) ? $data : ''; ?>

            google.charts.load("current", {packages:["corechart"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                for(let i = 0; i < datas.boardingPerState.length; i++) {
                    boardingPerState.push([datas.boardingPerState[i].state, datas.boardingPerState[i].quantity]);
                }
                var data = google.visualization.arrayToDataTable(boardingPerState);

                var options = {
                    title: 'Penionistas por estado',
                    pieHole: 0.4,
                };

                var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                chart.draw(data, options);


                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    let names = ['Year'];
                    for(let i = 0; i < datas.monthlyBillsPerYear.length; i++) {
                        names.push(datas.monthlyBillsPerYear[i].name);
                    }


                    let monthlyBillsPerYear = [names];
                    for(let i = 0; i < datas.monthlyBillsPerYear.length; i++) {
                        monthlyBillsPerYear.push([
                            datas.monthlyBillsPerYear[i].year.toString(),
                            parseInt(datas.monthlyBillsPerYear[i].value.replace(',00', '')),
                            (i === 1 ? 10 : 30)
                        ]);
                    }

                    console.log(monthlyBillsPerYear)
                    console.log(datas)
                    var data = google.visualization.arrayToDataTable(monthlyBillsPerYear);

                    var options = {
                        title: 'Gastos anuais',
                        curveType: 'function',
                        legend: { position: 'bottom' }
                    };

                    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                    chart.draw(data, options);
                }
            }
        </script>
    </head>
@endsection

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div id="donutchart" class="col-md-6" style="width: 500px; height: 300px;"></div>
    <div class="col-md-6">
        <table style="background: white;" class="table table-borderless">
            <p>Pagamentos no mês</p>
            <thead>
            <tr>
                <th scope="col">Pagamentos</th>
                <th scope="col">Valor</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Efetuados</th>
                <td>{{$paymentPerMonth === null ? 0 : $paymentPerMonth}}</td>
            </tr>
            <tr>
                <th scope="row">Atrasados</th>
                <td>{{$paymentLatePerMonth}}</td>
            </tr>
            </tbody>
        </table>
        <table class="table table-borderless" style="background: white;">
            <p>Total de pagamentos do mês</p>
            <thead>
            <tr>
                <th scope="col">Total a receber</th>
                <th scope="col">{{$totalReceivedPaymentPerMonth+$totalLatePaymentPerMonth}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Total recebido</th>
                <td>{{$totalReceivedPaymentPerMonth === null ? 0 : $totalReceivedPaymentPerMonth}}</td>
            </tr>
            <tr>
                <th scope="row">Total atrasado</th>
                <td>{{$totalLatePaymentPerMonth === null ? 0 : $totalLatePaymentPerMonth}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="curve_chart" class="col-md-12" style="width: 1000px; height: 470px"></div>
@stop
