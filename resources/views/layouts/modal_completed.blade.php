<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">@isset($title){{$title}}@endisset</h4>
</div>
<div class="modal-body">
    Você está alterando o status da melhoria para completo. Você tem certeza disso?
</div>
<div class="modal-footer">
    <form action="{{(isset($route)? $route : '')}}">
        <button type="submit" class="btn btn-success">Sim</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
    </form>
</div>