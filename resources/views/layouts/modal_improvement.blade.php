<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">@isset($title){{$title}}@endisset</h4>
</div>
<div class="modal-body">
    Você tem certeza que deseja {{$improvement}} a melhoria @isset($item) {{$item}} @endisset?
</div>
<div class="modal-footer">
    <form action="{{(isset($route)? $route : '')}}" method="{{($improvement == 'rejeitar') ? 'post' : 'get'}}">
        @if($improvement == 'rejeitar')
            {{csrf_field()}}
            <p class="p-improvement">Qual o motivo da rejeição?</p>
            <div class="col-md-12 form-group">
                <textarea class="form-control improvement" name="reason" rows="5"></textarea>
            </div>
        @endif
        <button type="submit" class="btn btn-success">Sim</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
    </form>
</div>
<style>
.improvement {
    margin-left: -2% !important;
}

.p-improvement {
    text-align: left;
}
</style>