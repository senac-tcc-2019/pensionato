<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pagamento de mensalidade</title>
</head>
<body>
    <div class="container">
        <h3>Pagamento de mensalidade</h3>
        <p>{{$payment->boarder->name}}, seu boleto vence em 10 dias, após esta data você pagará {{$config->interest_percentage}} % de juros</p>
        <a>Clique aqui para gerar o boleto</a>
    </div>
</body>
</html>
