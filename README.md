# Sistema gerenciador de pensionatos


### Para rodar o projeto é necessário


*  composer  
*  php 7.1.3
*  apache2
*  clonar o projeto
*  configurar o .env e a base de dados


### Dentro da pasta do projeto rodar os seguintes comandos


`php artisan:migrate`

`php artisan db:seed`

`php artisan serve`


### Projeto disponível em:

[Acesse GerenciaPensionato](http://pensionato.herokuapp.com/login)

### Credenciais:

Usuário: Professor

Senha: professor2019