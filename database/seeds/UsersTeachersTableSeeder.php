<?php

use App\Models\Address;
use App\Models\BoardingSchool;
use App\Models\Responsable;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersTeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Address::truncate();
        Responsable::truncate();
        BoardingSchool::truncate();

        DB::table('responsables_has_users')->truncate();

        User::create([
        	'name' => 'Professor',
        	'email' => 'francinemoraes08@hotmail.com',
        	'role' => 'administrador',
            'password' => bcrypt('professor2019')
        ]);

        User::create([
            'name' => 'Administrador',
            'email' => 'francinemoraes08@hotmail.com',
            'role' => 'administrador',
            'password' => bcrypt('fran0810')
        ]);

        Address::create([
            'cep' => '96300-000',
            'street' => 'Miguel Delelis',
            'number' => '921',
            'district' => 'Kennedy',
            'city' => 'Jaguarão',
            'state' => 'RS'
        ]);

        Responsable::create([
            'name' => 'Francine',
            'cpf' => '033.009.730-07',
            'rg' => '4124307473',
            'phone' => '(53) 98493-7928',
            'facebook' => 'DeathKaileena',
            'whatsapp' => '(53) 98493-7928',
            'email' => 'francinemoraes08@hotmail.com',
            'addresses_id' => 1
        ]);

        BoardingSchool::create([
            'name' => 'Francine',
            'responsables_id' => 1,
            'addresses_id' => 1,
        ]);

        DB::table('responsables_has_users')->insert([
            'responsables_id' => 1,
            'users_id' => 1
        ]);
    }
}
