<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeyWordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('key_words')->truncate();

        DB::table('key_words')->insert([
            'word' => 'Melhoria'
        ]);

        DB::table('key_words')->insert([
            'word' => 'Melhorias'
        ]);

        DB::table('key_words')->insert([
            'word' => 'para'
        ]);

        DB::table('key_words')->insert([
            'word' => 'pra'
        ]);

        DB::table('key_words')->insert([
            'word' => 'de'
        ]);

        DB::table('key_words')->insert([
            'word' => 'da'
        ]);

        DB::table('key_words')->insert([
            'word' => 'do'
        ]);

        DB::table('key_words')->insert([
            'word' => 'casa'
        ]);

        DB::table('key_words')->insert([
            'word' => 'banheiro'
        ]);

        DB::table('key_words')->insert([
            'word' => 'quarto'
        ]);

        DB::table('key_words')->insert([
            'word' => 'sala'
        ]);

        DB::table('key_words')->insert([
            'word' => 'este'
        ]);

        DB::table('key_words')->insert([
            'word' => 'esta'
        ]);

        DB::table('key_words')->insert([
            'word' => 'isto'
        ]);

        DB::table('key_words')->insert([
            'word' => 'isso'
        ]);

        DB::table('key_words')->insert([
            'word' => 'esse'
        ]);

        DB::table('key_words')->insert([
            'word' => 'essa'
        ]);

        DB::table('key_words')->insert([
            'word' => 'aquele'
        ]);

        DB::table('key_words')->insert([
            'word' => 'aquela'
        ]);

        DB::table('key_words')->insert([
            'word' => 'no'
        ]);

        DB::table('key_words')->insert([
            'word' => 'na'
        ]);

        DB::table('key_words')->insert([
            'word' => 'depois'
        ]);

        DB::table('key_words')->insert([
            'word' => 'a'
        ]);

        DB::table('key_words')->insert([
            'word' => 'o'
        ]);

        DB::table('key_words')->insert([
            'word' => 'e'
        ]);

        DB::table('key_words')->insert([
            'word' => 'ou'
        ]);

        DB::table('key_words')->insert([
            'word' => 'quer'
        ]);

        DB::table('key_words')->insert([
            'word' => 'com'
        ]);

        DB::table('key_words')->insert([
            'word' => 'como'
        ]);

        DB::table('key_words')->insert([
            'word' => 'se'
        ]);

        DB::table('key_words')->insert([
            'word' => 'por'
        ]);

        DB::table('key_words')->insert([
            'word' => 'nesse'
        ]);

        DB::table('key_words')->insert([
            'word' => 'neste'
        ]);

        DB::table('key_words')->insert([
            'word' => 'ali'
        ]);

        DB::table('key_words')->insert([
            'word' => 'lá'
        ]);

        DB::table('key_words')->insert([
            'word' => 'pouco'
        ]);

        DB::table('key_words')->insert([
            'word' => 'pensionato'
        ]);

        DB::table('key_words')->insert([
            'word' => 'pátio'
        ]);

        DB::table('key_words')->insert([
            'word' => 'por quê'
        ]);

        DB::table('key_words')->insert([
            'word' => 'porquê'
        ]);

        DB::table('key_words')->insert([
            'word' => 'porque'
        ]);

        DB::table('key_words')->insert([
            'word' => 'por que'
        ]);

        DB::table('key_words')->insert([
            'word' => 'depois'
        ]);

        DB::table('key_words')->insert([
            'word' => 'mas'
        ]);

        DB::table('key_words')->insert([
            'word' => 'mais'
        ]);

        DB::table('key_words')->insert([
            'word' => 'menos'
        ]);

        DB::table('key_words')->insert([
            'word' => 'quero'
        ]);

        DB::table('key_words')->insert([
            'word' => 'ainda'
        ]);

        DB::table('key_words')->insert([
            'word' => 'preciso'
        ]);

        DB::table('key_words')->insert([
            'word' => 'lugar'
        ]);

        DB::table('key_words')->insert([
            'word' => 'em'
        ]);

        DB::table('key_words')->insert([
            'word' => 'após'
        ]);

        DB::table('key_words')->insert([
            'word' => 'que'
        ]);

        DB::table('key_words')->insert([
            'word' => 'nem'
        ]);

        DB::table('key_words')->insert([
            'word' => 'mesmo'
        ]);

        DB::table('key_words')->insert([
            'word' => 'quanto'
        ]);

        DB::table('key_words')->insert([
            'word' => 'também'
        ]);

        DB::table('key_words')->insert([
            'word' => 'é'
        ]);

        DB::table('key_words')->insert([
            'word' => 'são'
        ]);

        DB::table('key_words')->insert([
            'word' => 'adiante'
        ]);

        DB::table('key_words')->insert([
            'word' => 'aqui'
        ]);

        DB::table('key_words')->insert([
            'word' => 'além'
        ]);

        DB::table('key_words')->insert([
            'word' => 'antes'
        ]);

        DB::table('key_words')->insert([
            'word' => 'assim'
        ]);

        DB::table('key_words')->insert([
            'word' => 'nessa'
        ]);

        DB::table('key_words')->insert([
            'word' => '.'
        ]);

        DB::table('key_words')->insert([
            'word' => ','
        ]);

        DB::table('key_words')->insert([
            'word' => ';'
        ]);

    }
}
