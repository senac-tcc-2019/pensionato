<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('contacts')->insert(
            [
                'name' => 'Gás',
                'phone' => '(53) 98493-7928',
                'boarding_school_id' => 1
            ],
            [
                'name' => 'Circulus',
                'phone' => '(53) 98400-0000',
                'boarding_school_id' => 1
            ],
            [
                'name' => 'Faxineira',
                'phone' => '(53) 98400-0000',
                'boarding_school_id' => 1
            ]
        );

        DB::table('improvements')->insert(
            [
                'title' => 'Pia nova para casa',
                'approved' => 1,
                'completed' => 0,
                'votes' => 7,
                'users_id' => 1
            ],
            [
                'title' => 'Desentupir chuveiro',
                'approved' => 1,
                'completed' => 1,
                'votes' => 10,
                'users_id' => 1
            ],
            [
                'title' => 'Trocar vaso',
                'approved' => 0,
                'completed' => 0,
                'votes' => 7,
                'reason' => 'Não há fundos o suficiente',
                'users_id' => 2
            ]
        );

        DB::table('monthly_bills')->insert(
            [
                'name' => 'Água',
                'value' => 100.00,
                'pattern' => 1,
                'status' => 1,
                'boarding_school_id' => 1
            ],
            [
                'name' => 'Luz',
                'value' => 200.00,
                'pattern' => 1,
                'status' => 1,
                'boarding_school_id' => 1
            ]
        );

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
