<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $this->truncate();
        $this->call(UsersTeachersTableSeeder::class);
        $this->call(KeyWordsTableSeeder::class);
        $this->call(DatasTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    private function truncate()
    {
        $tables = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();

        foreach($tables as $table) {
            if($table == 'migrations') {
                continue;
            }

            DB::table($table)->truncate();
        }
    }
}
