<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardingSchoolHasBedroomsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'boarding_school_has_bedrooms';

    /**
     * Run the migrations.
     * @table boarding_school_has_bedrooms
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('boarding_school_id');
            $table->integer('bedrooms_id')->unsigned();

            $table->index(["boarding_school_id"], 'fk_boarding_school_has_bedrooms_boarding_school1_idx');

            $table->index(["bedrooms_id"], 'fk_boarding_school_has_bedrooms_bedrooms1_idx');


            $table->foreign('boarding_school_id', 'fk_boarding_school_has_bedrooms_boarding_school1_idx')
                ->references('id')->on('boarding_school');

            $table->foreign('bedrooms_id', 'fk_boarding_school_has_bedrooms_bedrooms1_idx')
                ->references('id')->on('bedrooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
