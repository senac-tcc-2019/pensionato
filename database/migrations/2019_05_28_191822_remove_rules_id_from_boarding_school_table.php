<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRulesIdFromBoardingSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boarding_school', function (Blueprint $table) {
            $table->dropForeign('fk_boarding_school_rules1_idx');
            $table->dropColumn('rules_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boarding_school', function (Blueprint $table) {
            $table->integer('rules_id')->unsigned();
        });
    }
}
