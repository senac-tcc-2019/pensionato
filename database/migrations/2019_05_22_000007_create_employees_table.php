<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'employees';

    /**
     * Run the migrations.
     * @table employees
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('phone', 191);
            $table->string('cpf', 191);
            $table->string('rg', 191);
            $table->string('ctps', 191);
            $table->string('function', 191);
            $table->double('salary');
            $table->integer('addresses_id')->unsigned();

            $table->index(["addresses_id"], 'fk_employees_addresses1_idx');
            $table->timestamps();


            $table->foreign('addresses_id', 'fk_employees_addresses1_idx')
                ->references('id')->on('addresses')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
