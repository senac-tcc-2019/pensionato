<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveBoardersIdFromMonthlyBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('monthly_bills', function (Blueprint $table) {
            $table->dropForeign('fk_monthly_bills_boarders1_idx');
            $table->dropColumn('boarders_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monthly_bills', function (Blueprint $table) {
            $table->integer('boarders_id')->unsigned();

            $table->index(["boarders_id"], 'fk_monthly_bills_boarders1_idx');


            $table->foreign('boarders_id', 'fk_monthly_bills_boarders1_idx')
                ->references('id')->on('boarders')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }
}
