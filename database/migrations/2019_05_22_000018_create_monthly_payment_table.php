<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyPaymentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'monthly_payment';

    /**
     * Run the migrations.
     * @table monthly_payment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('predicted_data');
            $table->double('predicted_price');
            $table->date('payment_date');
            $table->double('payment_price')->nullable();
            $table->double('penalty');
            $table->double('interest');
            $table->integer('boarders_id')->unsigned();

            $table->index(["boarders_id"], 'fk_monthly_payment_boarders1_idx');


            $table->foreign('boarders_id', 'fk_monthly_payment_boarders1_idx')
                ->references('id')->on('boarders')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
