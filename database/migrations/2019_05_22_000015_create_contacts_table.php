<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contacts';

    /**
     * Run the migrations.
     * @table contacts
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('phone', 191);
            $table->integer('status')->default('1');
            $table->integer('boarding_school_id')->unsigned();

            $table->index(["boarding_school_id"], 'fk_contacts_boarding_school1_idx');
            $table->timestamps();


            $table->foreign('boarding_school_id', 'fk_contacts_boarding_school1_idx')
                ->references('id')->on('boarding_school')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
