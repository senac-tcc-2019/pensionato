<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprovementsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'improvements';

    /**
     * Run the migrations.
     * @table improvements
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 191);
            $table->boolean('approved')->default('0');
            $table->boolean('completed')->default('0');
            $table->integer('votes')->nullable();
            $table->longText('reason')->nullable();
            $table->integer('status')->default('1');
            $table->integer('users_id')->unsigned();

            $table->index(["users_id"], 'fk_improvements_users1_idx');


            $table->foreign('users_id', 'fk_improvements_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
