<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardingSchoolTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'boarding_school';

    /**
     * Run the migrations.
     * @table boarding_school
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->integer('status')->default('1');
            $table->integer('responsables_id')->unsigned();
            $table->integer('addresses_id')->unsigned();
            $table->integer('rules_id')->unsigned();

            $table->index(["responsables_id"], 'fk_boarding_school_responsables1_idx');

            $table->index(["addresses_id"], 'fk_boarding_school_addresses1_idx');

            $table->index(["rules_id"], 'fk_boarding_school_rules1_idx');
            $table->timestamps();


            $table->foreign('responsables_id', 'fk_boarding_school_responsables1_idx')
                ->references('id')->on('responsables')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('addresses_id', 'fk_boarding_school_addresses1_idx')
                ->references('id')->on('addresses')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rules_id', 'fk_boarding_school_rules1_idx')
                ->references('id')->on('rules')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
