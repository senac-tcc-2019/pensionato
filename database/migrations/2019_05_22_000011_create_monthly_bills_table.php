<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyBillsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'monthly_bills';

    /**
     * Run the migrations.
     * @table monthly_bills
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->double('value');
            $table->boolean('pattern')->default('0');
            $table->integer('status')->default('1');
            $table->integer('boarders_id')->unsigned();

            $table->index(["boarders_id"], 'fk_monthly_bills_boarders1_idx');
            $table->timestamps();


            $table->foreign('boarders_id', 'fk_monthly_bills_boarders1_idx')
                ->references('id')->on('boarders')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
