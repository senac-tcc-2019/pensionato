<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressIdOnBoarders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boarders', function (Blueprint $table) {
            $table->integer('addresses_id')->unsigned()->nullable();
        });

        Schema::table('boarders', function (Blueprint $table) {
            $table->foreign('addresses_id')
                ->references('id')->on('addresses')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
