<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'boarders';

    /**
     * Run the migrations.
     * @table boarders
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('cpf', 191);
            $table->string('rg', 191);
            $table->string('course', 191);
            $table->string('institution', 191);
            $table->string('phone', 191);
            $table->string('facebook', 191)->nullable();
            $table->string('whatsapp', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->date('payment_day');
            $table->tinyInteger('status')->default('1');
            $table->integer('responsables_id')->unsigned();
            $table->integer('bedrooms_id')->unsigned();
            $table->integer('contracts_id')->unsigned();
            $table->integer('users_id')->unsigned();

            $table->index(["users_id"], 'fk_boarders_users1_idx');

            $table->index(["bedrooms_id"], 'fk_boarders_bedrooms1_idx');

            $table->index(["contracts_id"], 'fk_boarders_contracts1_idx');

            $table->index(["responsables_id"], 'fk_boarders_responsables_idx');
            $table->timestamps();


            $table->foreign('responsables_id', 'fk_boarders_responsables_idx')
                ->references('id')->on('responsables')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('bedrooms_id', 'fk_boarders_bedrooms1_idx')
                ->references('id')->on('bedrooms')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('contracts_id', 'fk_boarders_contracts1_idx')
                ->references('id')->on('contracts')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('users_id')
                ->references('id')->on('boarders')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
