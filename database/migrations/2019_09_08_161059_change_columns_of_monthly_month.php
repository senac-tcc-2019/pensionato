<?php

use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\Type;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsOfMonthlyMonth extends Migration
{
     /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'monthly_payment';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Type::hasType('double')) {
            Type::addType('double', FloatType::class);
        }
        
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->date('payment_date')->nullable()->change();
            $table->double('payment_price')->nullable()->change();
            $table->double('penalty')->nullable()->change();
            $table->double('interest')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->date('payment_date')->change();
            $table->double('payment_price')->change();
            $table->double('penalty')->change();
            $table->double('interest')->change();
        });
    }
}
