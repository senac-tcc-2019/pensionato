<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsablesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'responsables';

    /**
     * Run the migrations.
     * @table responsables
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('cpf', 191);
            $table->string('rg', 191);
            $table->string('phone', 191);
            $table->string('facebook', 191)->nullable();
            $table->string('whatsapp', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->tinyInteger('status')->default('1');
            $table->integer('users_id')->unsigned();
            $table->integer('addresses_id')->unsigned();

            $table->index(["addresses_id"], 'fk_responsables_addresses1_idx');
            $table->timestamps();


            $table->foreign('addresses_id', 'fk_responsables_addresses1_idx')
                ->references('id')->on('addresses')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('users_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
