<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBoardingHasBedroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('boarding_school_has_bedrooms', function (Blueprint $table) {
            $table->dropForeign('fk_boarding_school_has_bedrooms_boarding_school1_idx');
            $table->dropColumn('boarding_school_id');
        });

        Schema::table('boarding_school_has_bedrooms', function (Blueprint $table) {
            $table->integer('boarding_school_id')->unsigned();

            
        });


        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
