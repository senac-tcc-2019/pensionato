<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardingSchoolHasRoomsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'boarding_school_has_rooms';

    /**
     * Run the migrations.
     * @table boarding_school_has_rooms
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('boarding_school_id');
            $table->integer('rooms_id')->unsigned();

            $table->index(["rooms_id"], 'fk_boarding_school_has_rooms_rooms1_idx');

            $table->index(["boarding_school_id"], 'fk_boarding_school_has_rooms_boarding_school1_idx');


            $table->foreign('boarding_school_id', 'fk_boarding_school_has_rooms_boarding_school1_idx')
                ->references('id')->on('boarding_school');

            $table->foreign('rooms_id', 'fk_boarding_school_has_rooms_rooms1_idx')
                ->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
