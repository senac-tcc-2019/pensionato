<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tasks';

    /**
     * Run the migrations.
     * @table tasks
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('description');
            $table->boolean('finished')->default('0');
            $table->string('status', 45);
            $table->integer('boarders_id')->unsigned();
            $table->integer('employees_id')->unsigned();

            $table->index(["boarders_id"], 'fk_tasks_boarders1_idx');

            $table->index(["employees_id"], 'fk_tasks_employees1_idx');
            $table->timestamps();


            $table->foreign('boarders_id', 'fk_tasks_boarders1_idx')
                ->references('id')->on('boarders')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('employees_id', 'fk_tasks_employees1_idx')
                ->references('id')->on('employees')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
