<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardersHasBoardingSchoolTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'boarders_has_boarding_school';

    /**
     * Run the migrations.
     * @table boarders_has_boarding_school
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('boarders_id');
            $table->integer('boarding_school_id')->unsigned();

            $table->index(["boarding_school_id"], 'fk_boarders_has_boarding_school_boarding_school1_idx');

            $table->index(["boarders_id"], 'fk_boarders_has_boarding_school_boarders1_idx');


            $table->foreign('boarders_id', 'fk_boarders_has_boarding_school_boarders1_idx')
                ->references('id')->on('boarders');

            $table->foreign('boarding_school_id', 'fk_boarders_has_boarding_school_boarding_school1_idx')
                ->references('id')->on('boarding_school');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
